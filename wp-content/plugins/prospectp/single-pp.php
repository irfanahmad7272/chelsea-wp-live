
<?php
print_r($post->post_content);
?>
<script async defer src="https://connect.facebook.net/en_US/sdk.js"></script>
<script>
    setTimeout(function(){
        let markup  = document.documentElement.innerHTML;
        let domVars = markup.match(/(?<={{\s*).*?(?=\s*}})/gs);
        let urlVars = getUrlVars();
        for(let urlVar in urlVars){
            for(let domVar of domVars){
                if(urlVar === domVar){
                    let elems = document.body.children;
                    for(let i = 0; i < elems.length; i++){
                        let usIt = urlVars[urlVar];
                        elems[i].innerHTML = elems[i].innerHTML.replace("{{"+domVar+"}}", decodeURI(usIt));
                    }
                }
            }
        }
    }, 0);

    function getUrlVars() {
        let vars = {};
        let parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
            vars[key] = value;
        });
        return vars;
    }

    function changeVar(facebookData){
        let vars = {
            full_name: 'name',
            first_name: 'first_name',
            last_name: 'last_name'
        };
        let markup  = document.documentElement.innerHTML;
        let domVars = markup.match(/(?<={{\s*).*?(?=\s*}})/gs); //this work
        for(let domVar of domVars){
            for(let _var in vars){
                if(domVar === _var){
                    let elems = document.body.children;
                    for(let i = 0; i < elems.length; i++){
                        elems[i].innerHTML = elems[i].innerHTML.replace("{{"+domVar+"}}", facebookData[vars[_var]]);
                    }
                }
            }

        }
    }

    let url   = new URL(window.location.href);
    let type  = url.searchParams.get("type");
    let appId = url.searchParams.get("app-id");
    if(type === 'facebook'){
        let facebookData = JSON.parse(localStorage.getItem("ProspectPluginFacebook"));
        if(!facebookData){
            window.fbAsyncInit = function(){
                FB.init({
                    appId: appId, //The facebook application id
                    autoLogAppEvents: true,
                    cookie: true,
                    xfbml: true,
                    version: 'v6.0'
                });
                FB.login(function(response){
                    if(response.authResponse){
                        FB.api('/me', {fields: ['last_name', "first_name", "name"]}, function(response){
                            if(response.last_name){
                                facebookData = response;
                                localStorage.setItem("ProspectPluginFacebook", JSON.stringify(response));
                                changeVar(facebookData);
                            }
                            else{
                                console.log('Data incomplete.');
                            }
                        });
                    }else{
                        console.log('User cancelled login or did not fully authorize.');
                    }
                });
            };
        }
        else{
            changeVar(facebookData);
        }
    }


</script>

