<?php

namespace traits;

trait Requests{

    public function saveTemplateInAPI($request_url, $token, $data){
        if($token === 'noToken'){
            echo 'noToken';
        }else{
            $ch = curl_init(APIURL.$request_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'apiToken: ' .$token,
            ]);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $server_output  = curl_exec($ch);
            print_r($server_output);//Don't remove this, need to show messages with ajax
            curl_close($ch);
        }
    }

    public function getUserTemplateData($requestURL, $user_token, $domain, $page_type){
        $data = [
            'tmp_id' => $this->getTmpID($page_type, 'get_number'),
            'domain' => $domain
        ];
        $data = json_encode($data);
        $ch = curl_init(APIURL . $requestURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$user_token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);
        $server_output = json_decode(substr(stripcslashes(stripcslashes($server_output)), 1, -1), true);
        return $server_output;
    }

    public function getVariables($page, $domain, $token){
        $data = [
            'page' => $page,
            'domain' => $domain,
        ];
        $data = json_encode($data);
        $ch = curl_init(APIURL . 'get_user_template_data');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);
        $server_output = json_decode(stripcslashes($server_output), true);
        return $server_output;
    }

    public function updateSubscriptionPlan($request_url, $token, $data){
        if($token === 'noToken'){
            echo 'noToken';
        }else{
            $ch = curl_init(APIURL . $request_url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'apiToken: ' . $token,
            ]);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $server_output = curl_exec($ch);
            curl_close($ch);
            return $server_output;
        }
    }

    public function getUserSlugs($token, $data){
        $ch = curl_init(APIURL.'get_user_slugs');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function getPlansFromAPI(){
        $apiToken = $this->getSessionApiToken();
        if($apiToken === 'noToken'){
            echo 'noToken';
        }else{
            $ch = curl_init(APIURL . 'get_plans');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'apiToken: ' . $apiToken,
            ]);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
//            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $server_output = curl_exec($ch);
            print_r($server_output);
            curl_close($ch);
        }
    }

    public function registerApiUser($request){
        $data['name'] = $request['name'];
        $data['email'] = $request['email'];
        $data['password'] = $request['password'];

        $ch = curl_init(APIURL.'register');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);
        return json_decode($server_output, true);
    }

    public function loginApiUser($request){
        $data['email'] = $request['email'];
        $data['password'] = $request['password'];
        $data['domain'] = $_SERVER['HTTP_HOST'];

        $ch = curl_init(APIURL.'login');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);

        if(curl_error($ch)){
            print_r('Error:' . curl_error($ch));
            die();
        }
//        else if(curl_errno($ch)){
//            print_r(curl_errno($ch) . 'errno');
//            die();
//        }
//        var_dump($server_output . '//////data');
//        var_dump(json_decode($server_output, true) . 'data');
//        die();
        curl_close($ch);
        return json_decode($server_output, true);
    }

    public function updateApiEmail($request, $token){
        $ch = curl_init(APIURL.'update_email');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
            'email: ' . $request['email']
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output = curl_exec($ch);

        if(curl_error($ch)){
            echo 'Error:' . curl_error($ch);
        }
        else{
            curl_close($ch);
//            print_r($server_output);
//            die();
            return json_decode($server_output, true);
        }
    }

    public function updatePasswordApi($request, $token){
        $ch = curl_init(APIURL.'update_password');
        $data = ['old_password' => $request['old_password'], 'new_password' => $request['new_password']];
        $data = json_encode($data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output = curl_exec($ch);

        if(curl_error($ch)){
            echo 'Error:' . curl_error($ch);
        }
        else{
            curl_close($ch);
            return json_decode($server_output, true);
        }
    }

    public function updateIntegrationsApi($request, $token){
        $ch = curl_init(APIURL.'update_integrations');
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
            'getResponseApi: ' . $request['get_response_api'],
            'sendlaneApi: ' . $request['sendlane_api'],
            'aweberApi: ' . $request['aweber_api']
        ]);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output = curl_exec($ch);

        if(curl_error($ch)){
            echo 'Error:' . curl_error($ch);
        }
        else{
            curl_close($ch);
            return json_decode($server_output, true);
        }
    }

    public function getIntDataApi($token)
    {
        $ch = curl_init(APIURL.'get_int_data');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function getUserSitesApi($token){
        $ch = curl_init(APIURL.'get_user_sites');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);

        return $server_output;
    }

    public function getUserDataApi($token)
    {
        $ch = curl_init(APIURL.'get_user_data');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function pwdResetApi($request){
        $data = [
            'email'    => $request['email'],
            'password' => $request['password'],
            'password_confirmation' => $request['password_confirmation'],
            'token'    => $request['token']
        ];

        $ch = curl_init(APIURL.'password/reset');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function resetForm($request){
        $data = [
            'email' => $request['email'],
            'host' => $_SERVER['HTTP_HOST']
        ];

        $ch = curl_init(APIURL.'password/email');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

        $server_output  = curl_exec($ch);
        curl_close($ch);

        return $server_output;
    }

    public function getUrlFromApi($request, $token){
        $ch = curl_init(APIURL . 'get_urls');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($request));
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function updateStatusToRevoked($request, $token){
        $data = json_encode([
            'id' => $request['site_id'],
            'password' => $request['password'],
            'status'  => 'revoked'
        ]);

        $response = $this->sendCurlToApiGet($token, 'update_site_status', $data);
        return $response;
    }

    public function getVisits($token, $limit, $offset){
        $ch = curl_init(APIURL . 'get_visits/'.$offset.'/' . $limit);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function getVisitsForCsv($start_date, $end_date){
        $ch = curl_init(APIURL . 'get_visits_for_csv/'.$start_date.'/' . $end_date);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$this->getSessionApiToken(),
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

    public function filterVisitsByDate($start_date, $end_date, $limit, $offset){
        $ch = curl_init(APIURL . 'get_filtered_visits/'.$start_date.'/'.$end_date.'/'.$offset.'/' . $limit);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$this->getSessionApiToken(),
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $server_output  = curl_exec($ch);
        curl_close($ch);
//        print_r($server_output);die();
        return $server_output;
    }

    public function getVisitsCount($startDate = 0, $end_date = 0){
        $ch = curl_init(APIURL . 'get_visits_count/' . $startDate . '/' . $end_date);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$this->getSessionApiToken(),
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
//        curl_setopt($ch, CURLOPT_POST, 1);
//        curl_setopt($ch, CURLOPT_POSTFIELDS, []);
        $server_output  = curl_exec($ch);
        curl_close($ch);
//        print_r($server_output);die();
        return $server_output;
    }

    public function saveVisitToApi($token, $request){
        $save = true;
        $urlArray = explode('pp', $_SERVER['REQUEST_URI']);
        $pageSlug = explode('/', $urlArray[1])[1];
        $esimInch = explode($pageSlug, $urlArray[1])[1];
        $afterPageSlug = substr($esimInch, 0, 1);
        if($afterPageSlug !== '/') $save = false;
        if(strpos($pageSlug, '?')) $save = false; //prevent double save
        if($save){
            //get post by slug
            $args = array(
                'name'        => $pageSlug,
                'post_type'   => 'pp',
                'post_status' => 'publish',
                'numberposts' => 1
            );
            $my_post = get_posts($args)[0];

            $postTitle = $my_post->post_title;

            $data = [
                'page_title' => $postTitle,
                'page_slug'  => $pageSlug,
                'domain'     => $_SERVER['HTTP_HOST'],
                'email'      => isset($request['email'])     ? $request['email']     : null,
                'name'       => isset($request['full_name']) ? $request['full_name'] : null,
                'company'    => isset($request['company'])   ? $request['company']   : null,
            ];

            $data = json_encode($data);

            $ch = curl_init(APIURL . 'create_visit');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, [
                'Content-Type: application/json',
                'apiToken: ' .$token,
            ]);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);

            $server_output  = curl_exec($ch);
            curl_close($ch);
//            print_r($server_output);
        }
    }

    public function getSiteStatus($token, $userToken){
        $data = ['userToken' => $userToken];
        $data = json_encode($data);
        return $this->sendCurlToApiGet($token, 'check_active', $data);
    }

    public function sendCurlToApiGet($token, $url, $data){
        $ch = curl_init(APIURL . $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, [
            'Content-Type: application/json',
            'apiToken: ' .$token,
        ]);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $server_output  = curl_exec($ch);
        curl_close($ch);
        return $server_output;
    }

}