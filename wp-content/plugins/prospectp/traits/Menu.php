<?php

namespace traits;


trait Menu
{
    public function register_menu()
    {
        add_action('admin_menu', array($this, 'pp_admin_menu'));
        add_action('admin_menu', [$this, 'add_custom_link_into_pages_menu']);
    }

    // Add menu items to admin panel
    public function pp_admin_menu()
    {
        add_menu_page(
            'prospect',
            'Prospect',
            'manage_options',
            'prospect',
            null,
            'dashicons-admin-generic'
        );

        // Settings Page
        add_submenu_page(
            'prospect',
            'Prospect',
            'Prospect',
            'edit_posts',
            'prospect',
            array($this, 'prospect_view')
        );

// // Settings Page
        add_submenu_page(
            null,
            'Settings',
            'Settings',
            'edit_posts',
            'pp_settings',
            array($this, 'settings_view')
        );

        add_submenu_page(
            null,
            'Reports',
            'Reports',
            'edit_posts',
            'pp_reports',
            array($this, 'reporting_view')
        );

        add_submenu_page(
            null,
            'Login',
            'Login',
            'edit_posts',
            'login',
            array($this, 'login_view')
        );

        add_submenu_page(
            null,
            'PasswordReset',
            'PasswordReset',
            'edit_posts',
            'password_reset',
            array($this, 'password_reset_view')
        );

        add_submenu_page(
            null,
            'PasswordResetPage',
            'PasswordResetPage',
            'edit_posts',
            'password_reset_form',
            array($this, 'password_reset_form_view')
        );

        add_submenu_page(
            null,
            'logout',
            'logout',
            'edit_posts',
            'logout1122',
            array($this, 'logout1122')
        );


    }

    function prospect_view(){
        $check = $this->checkStatusOrRedirect();
        if($check){
            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            require_once __DIR__ . '/../views/admin/templates.php';
        }
    }

    function logout1122(){
        setcookie('userToken', '', time() - 3600);
        setcookie('expiryDate', '', time() - 3600);
        setcookie('loggedIn', '', time() - 3600);

        if(isset($_COOKIE['userToken']) && isset($_COOKIE['expiryDate'])){
            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            require_once __DIR__ . '/../views/admin/templates.php';
        }
        else{
            $this->register_front_styles_scripts();
            wp_redirect(get_site_url() . '/wp-admin/admin.php?page=login');
        }
    }

    public function settings_view(){
        $check = $this->checkStatusOrRedirect();
        if($check){
            $this->register_front_styles_scripts();
            $userPpPosts   = $this->get_user_posts();
            $userData      = $this->get_user_data();
            $ints          = $this->get_int_data();
            $userSitesData = $this->get_user_sites();
            $subPlan       = $userSitesData['subscriptionPlan'];

            require_once __DIR__ . '/../views/admin/settings.php';
        }
    }

    public function login_view(){
        if(isset($_COOKIE['userToken']) && isset($_COOKIE['expiryDate'])){
            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            wp_redirect(get_site_url() . '/wp-admin/admin.php?page=prospect');
        }
        else{
            $this->register_front_styles_scripts();
            require_once __DIR__ . '/../views/admin/login.php';
        }
    }

    public function password_reset_view(){
        if(isset($_COOKIE['userToken']) && isset($_COOKIE['expiryDate'])){
            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            wp_redirect(get_site_url() . '/wp-admin/admin.php?page=prospect');
        }
        else{
            $this->register_front_styles_scripts();
            require_once __DIR__ . '/../views/admin/password-reset.php';
        }
    }

    public function password_reset_form_view(){
        if(isset($_COOKIE['userToken']) && isset($_COOKIE['expiryDate'])){
            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            wp_redirect(get_site_url() . '/wp-admin/admin.php?page=prospect');
        }
        else{
            $this->register_front_styles_scripts();
            require_once __DIR__ . '/../views/admin/password-reset-page.php';
        }
    }

    public function reporting_view(){
        $check = $this->checkStatusOrRedirect();
        if($check){
            $start_date = isset($_GET['start_date']) ? $_GET['start_date'] : false;
            $end_date = isset($_GET['end_date']) ? $_GET['end_date'] : false;
            $filter = ($start_date && $end_date) ? true : false;
            $this->register_front_styles_scripts();
            $total = $filter ? $this->getVisitsCount($start_date, $end_date) : $this->getVisitsCount();

            // How many items to list per page
            $limit = 10;

            // How many pages will there be
            $pages = ceil(intval($total) / $limit);

            // What page are we currently on?
            $page = min($pages, filter_input(INPUT_GET, 'p', FILTER_VALIDATE_INT, array(
                'options' => array(
                    'default' => 1,
                    'min_range' => 1,
                ),
            )));

            // Calculate the offset for the query
            if($page != 0) $offset = ($page - 1) * $limit;
            else $offset = 0;

            // Some information to display to the user
            $start = $offset + 1;
            $end = min(($offset + $limit), $total);
            $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";

            $query = parse_url($actual_link, PHP_URL_QUERY);

            if($query){
                $actual_link .= '&p';
            }else{
                $actual_link .= '?p';
            }
            // The "back" link
            $prevlink = ($page > 1) ? '<a href="' . $actual_link . '=1" title="First page">&laquo;</a> <a href="' . $actual_link . '=' . ($page - 1) . '" title="Previous page">&lsaquo;</a>' : '<span class="disabled">&laquo;</span> <span class="disabled">&lsaquo;</span>';

            // The "forward" link

            $nextlink = ($page < $pages) ? '<a href="' . $actual_link . '=' . ($page + 1) . '" title="Next page">&rsaquo;</a> <a href="' . $actual_link . '=' . $pages . '" title="Last page">&raquo;</a>' : '<span class="disabled">&rsaquo;</span> <span class="disabled">&raquo;</span>';

            // Display the paging information
            $pagination = '<div id="paging"><p>' . $prevlink . ' Page ' . $page . ' of ' . $pages . ' pages, displaying ' . $start . '-' . $end . ' of ' . $total . ' results ' . $nextlink . ' </p></div>';

            $reports = $filter ? $this->filterVisitsByDate($start_date, $end_date, $limit, $offset) : $this->getVisits($this->getSessionApiToken(), $limit, $offset);
            $reports = json_decode($reports);

            $this->register_front_styles_scripts();
            $userPpPosts = $this->get_user_posts();
            require_once __DIR__ . '/../views/admin/reports.php';
        }
    }

    public function checkStatusOrRedirect(){
        if(isset($_COOKIE['userToken']) && isset($_COOKIE['expiryDate'])){
            $response = $this->getSiteStatus($this->getSessionApiToken(), $_COOKIE['userToken']);
            $response = json_decode($response, true);
            if($response['success'] === true){
                $siteStatus = $response['site_status'];
                if($siteStatus === 'active') return true;
                else{
                    $_SESSION['errors'] = "Your site is revoked or deleted.";
                    $this->removeCookiesAndRedirectLogin();
                }
            }
            else{
                $_SESSION['errors'] = $response['message'];
                $this->removeCookiesAndRedirectLogin();
            }
        }
        else{
            $_SESSION['errors'] = "Your session expired, please login again.";
            $this->removeCookiesAndRedirectLogin();
        }
    }

    public function removeCookiesAndRedirectLogin(){
        setcookie('userToken', '', time() - 3600);
        setcookie('expiryDate', '', time() - 3600);
        setcookie('loggedIn', '', time() - 3600);
        $this->register_front_styles_scripts();
        wp_redirect(get_site_url() . '/wp-admin/admin.php?page=login');
    }

    function add_custom_link_into_pages_menu() {
        global $submenu;
        $permalink = get_admin_url() . 'admin.php?page=templates';
        $submenu['edit.php?post_type=page'][] = array( 'Add With Prospect Plugin', 'manage_options', $permalink );
    }

}
