jQuery(function ($){
    function mainHeight(){

        var headerHeight = $("header").outerHeight(),
            footerHeight = $("footer").outerHeight(),
            bodyHeight = $("body").outerHeight();


        if($(window).width() > 1200){
            $("main").css('height', bodyHeight - (headerHeight + footerHeight));
        }else{
            $("main").css('min-height', bodyHeight - (headerHeight + footerHeight));
        }

    }

    mainHeight();

    $(window).resize(function(){
        mainHeight();
    });


    $('input[type="checkbox"]#paypal').change(function(){
        /* $(".animation-block").slideToggle("show")*/

        if(this.checked){
            $(".animation-block").slideDown(400);
            $(".pay-btn").css("visibility", "hidden");
            $(".paypal-btn").css("visibility", "visible");

        }else{
            $(".animation-block").slideUp(400);
            $(".pay-btn").css("visibility", "visible");
            $(".paypal-btn").css("visibility", "hidden");
        }

    });


    $(".changeps .bgc-pink").click(function(){
        $(".changepass").css("display", "block")
    });

});


