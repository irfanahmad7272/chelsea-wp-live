jQuery(function ($) {
    let datepicker = $('.datepicker');
    let filter_button = $('#filter_button');

    datepicker.datepicker({
        format: 'yyyy-mm-dd',
        // dateFormat: 'yy-mm-dd',
        todayHighlight: true,
        autoclose: true,
        clearBtn: true
    });

    datepicker.change(function(){
        validateInputs();
    });

    datepicker.keyup(function(){
        validateInputs();
    });

    datepicker.blur(function(e){
        validateInputs();
    });

    datepicker.keydown(function(e){
        let key = e.key;
        if(key !== '-' && key !== 'Backspace' && key !== 'Delete'
            && key !== 'ArrowLeft' && key !== 'ArrowRight' && key !== 'F5'
            && isNaN(parseInt(key))) e.preventDefault();
    });

    //validate as xxxx-xx-xx
    function isValidDate(dateString) {
        var regEx = /^\d{4}-\d{2}-\d{2}$/;
        return dateString.match(regEx) != null;
    }

    function validateInputs(){
        let valid = true;
        datepicker.each(function(){
            let _val = $(this).val();
            if(_val !== '') {
                valid = isValidDate(_val);
                if(valid) valid = true;
                else{
                    valid = false;
                    return false;
                }
            }
            else{
                valid = false;
                return false;
            }
        });
        if(valid === true) filter_button.prop('disabled', false);
        else filter_button.prop('disabled', true);
    }

});