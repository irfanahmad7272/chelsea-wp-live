jQuery(function ($) {
    let div_alert  = $('.div_alert');
    let alert_text = $('.alert_text');

    $('#email-submit').click(function(e){
        e.preventDefault();
        $('#email-form').submit();
    });

    $('#pwd_submit').click(function(e){
        e.preventDefault();
        $('#pwd_form').submit();
    });

    //-------------- - - Revoke - - ------------------
    $('.revoke-label').click(function(){
        let _this  = $(this);
        let siteId = _this.parent().find('.site-id').val();
        let passwordInput = $('.pwd-confirm');
        $('.revoke-label').removeClass('revoke-me');
        _this.addClass('revoke-me');

        $('.revoke-input').removeClass('border-orange');
        _this.parent().find('.revoke-input').addClass('border-orange');
        passwordInput.attr('data-site-id', siteId);
        passwordInput.parent().prop('hidden', false);
    });

    $('.pwd-confirm').click(function(e){
        e.preventDefault();
        let _this  = $(this);
        let siteId = _this.attr('data-site-id');
        let totalSitesSpan = $('#total_sites');
        let sitesCount = totalSitesSpan.text();
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            data: {
                action: 'update_status_to_revoked',
                site_id: siteId,
                password: $('#password1').val()
            },
            success: function(response){
                let data = JSON.parse(response);
                if(data.success === true){
                    $('.revoke-me').parent().remove();
                    _this.parent().prop('hidden', true);
                    totalSitesSpan.text(parseInt(sitesCount) - 1);
                    showAlertMessage(alert_text, div_alert, 'alert-success', data.message);
                }
                else showAlertMessage(alert_text, div_alert, 'alert-danger', data.message);
            }
        });
    });

    $('.cancel-link').click(function(e){
        e.preventDefault();
        let parent = $(this).parent();
        $('.revoke-input').removeClass('border-orange');
        parent.prop('hidden', true);
        parent.find('.pwd-confirm').attr('data-site-id', '');
    });
    //-------------- - - / Revoke - - ------------------


    // -------------- - - Upgrade/Downgrade - - ------------------
    $('.change-plan').click(function(e){
        e.preventDefault();
        $('#sub-plan-id').val($(this).attr('data-plan-id'));
        $('#form-sub-plan').submit();
    });
    // -------------- - - / Upgrade/Downgrade - - ------------------

    //----------------------- - - functions - - -----------------------
    function showAlertMessage(alertDivElem, parentDivElem, alert_class, message) {
        parentDivElem.removeProp('hidden');
        alertDivElem.addClass(alert_class).html(message);
        alertDivElem.show(800);
        setTimeout(function () {
            alertDivElem.hide(800);
        }, 3000);
        setTimeout(function () {
            alertDivElem.removeClass(alert_class);
        }, 3800);
    }
    //----------------------- - - / functions - - -----------------------
});