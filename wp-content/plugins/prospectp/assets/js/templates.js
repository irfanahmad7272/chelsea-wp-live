jQuery(function ($) {

    //---------------- - - variables - - --------------------
    let template_types  = $('.template-types');
    let alert_text_elem = $('.alert_text');
    let div_alert_elem  = $('.div_alert');
    let update_template = $('#update_template');
    let newTmpUnsaved   = false;
    let newTmpExist     = false;
    let saveClicked     = false;
    let previousUrl     = document.referrer;
    let protocol        = window.location.protocol;
    let urlsDiv         = $('#urls-div');
    let httpHost        = $("#http-host").val();
    let appIdInput      = $('#app-id');
    let copyUrlButton   = $('.copy-url');
    //---------------- - - /variables - - --------------------

    $('[data-toggle="tooltip"]').tooltip();

    update_template.prop('disabled', true);

    $(document).on('keyup', '#title', function () {
        $('.bgc-lblue').text($(this).val());
    });

    if(!saveClicked){
        setTimeout(function() {
            let full_url = window.location.href;
            if(full_url.indexOf('#new_template') !== -1){
                $('.new_tmp_button').trigger('click');
            }
            else if(full_url.indexOf('#') !== -1){
                let slug = window.location.href.split('#')[1];
                $(`a[data-slug="${slug}"]`).trigger('click');
            }
            else $('.template-links').first().trigger('click');
        },10);
    }

    template_types.click(function(){
        template_types.removeClass('active');
        $(this).addClass('active');
    });

    $('.new_tmp_button').click(function(e){
        e.stopPropagation();
        e.preventDefault();
        if(newTmpUnsaved){
            let check = confirm("Changes you made may not be saved.");
            if(check){
                newTmpUnsaved = false;
                newTmpExist   = false;
                $('.bottom-list li:nth-child(2)').remove();
                $('#templatesModal').modal('show');
            }
            else return false;
        }
        else $('#templatesModal').modal('show');
    });

    $('#select_template').click(function(e){
        e.preventDefault();
        let tab_content = $('.content-div');
        let tmp_type = $('a.active').attr('data-type');
        $('#tmp_type').val(tmp_type);
        let tmp_name = $('#template_name').val();
        let tmp_slug = $('#template_slug').val();
        $('#slug').val(tmp_slug);
        $('#title').val(tmp_name);
        let new_nav  = getNavItem(tmp_name, tmp_slug);
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            data: {
                action: 'select_template',
                tmp_name: tmp_type
            },
            dataType: 'html',
            success: function (data) {
                newTmpExist = true;
                newTmpUnsaved = true;

                $(document).find('.new-editor').remove();
                let editor = getCKEditor(tmp_slug, 'new-editor');
                $('#update_template').attr('data-id', tmp_slug);
                $(document).find('.template-links').removeClass('c-blue temp-btn c-blue bgc-lblue').addClass('c-black');
                // pagesList.prepend(new_nav);
                $('.list-header').after(new_nav);
                tab_content.append(editor);

                $('#post_id').val('');
                update_template.prop('disabled', false);
                initCkEditor(tmp_slug, data);

                $('#templatesModal').modal('hide');
                $('#ppLink').attr('href', 'javascript:void(0)');

                let array = window.location.href.split('#');
                window.location.href = array[0] + '#' + tmp_slug;
            }
        })
    });

    $(document).on('click', '.template-links', function(){

        if(newTmpUnsaved){
            let check = confirm("Changes you made may not be saved.");
            if(check){
                newTmpUnsaved = false;
                newTmpExist   = false;
                $('.bottom-list li:nth-child(2)').remove();
            }
            else return false;
        }

        let _this = $(this);
        if(_this.hasClass('shown-link')) return false;
        $(document).find('.template-links').removeClass('c-blue temp-btn c-blue bgc-lblue shown-link').addClass('c-black');
        _this.removeClass('c-black').addClass('c-blue temp-btn c-blue bgc-lblue shown-link');

        // _this.addClass('editor-opened');
        let tab_content = $('.content-div');
        let slug = $(this).attr('data-slug');
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'get',
            data: {
                action: 'get_post_content',
                post_slug: slug
            },
            dataType: 'html',
            success: function (data) {
                let pp_post_data = JSON.parse(data);
                let post_title = pp_post_data.post_title;
                let post_slug  = pp_post_data.post_name;
                let post_id = pp_post_data.ID;
                let url     = window.location.href;

                let array = url.split('#');
                window.location.href = array[0] + '#' + post_slug;

                $(document).find('.new-editor').remove();
                let editor = getCKEditor(post_slug, 'new-editor');

                $('.tab-pane').removeClass('active');
                tab_content.append(editor);

                update_template.prop('disabled', false);
                initCkEditor(post_slug, pp_post_data.post_content);
                //----
                $('#title').val(post_title);
                $('#slug').val(post_slug);
                $('#update_template').attr('data-id', post_slug);
                // $('#template_form').attr('data-id', post_slug);
                $('#ppLink').attr('href', protocol + '//' + httpHost + '/pp/' + post_slug);

                if(urlsDiv.hasClass('shown-true')){
                    urlsDiv.removeClass('shown-true');
                    urlsDiv.hide(1000);
                }

                $('#post_id').val(pp_post_data.ID);
            }
        })
    });

    $(document).on('click', '#update_template', function () {
        saveClicked = true;
        let _this = $(this);
        _this.prop('disabled', true);
        let post_slug = _this.attr('data-id');
        let allHTMLData = CKEDITOR.instances[post_slug + 'editor'].getData();
        let post_title  = $('#title').val();
        let new_post_slug = $('#slug').val();
        let post_id  = $('#post_id').val();

        let slug_valid  = validatePageSlug(new_post_slug);

        if (new_post_slug === '' || post_title === '') {
            _this.prop('disabled', false);
            showAlertMessage(alert_text_elem, div_alert_elem, 'alert-danger', 'Please check required inputs');
            return;
        } else if (slug_valid === null) {
            _this.prop('disabled', false);
            showAlertMessage(alert_text_elem, div_alert_elem, 'alert-danger', 'URL can contain only letters, numbers and "-" symbol');
            return;
        }

        $.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            data: {
                action: 'create_update_pp_post',
                content: allHTMLData,
                post_name: new_post_slug,
                post_title: post_title,
                tmp_type: $('#tmp_type').val(),
                page_id: post_id,
            },
            dataType: 'html',
            success: function(data){
                _this.prop('disabled', false);
                data = JSON.parse(data);
                if(data.success === true){
                    $('#ppLink').attr('href', protocol + '//' + httpHost + '/pp/' + new_post_slug);
                    if(newTmpExist){
                        newTmpUnsaved = false;
                        newTmpExist   = false;
                    }
                    showAlertMessage(alert_text_elem, div_alert_elem, 'alert-success', data.message);
                    $('#title').val(post_title);
                    $('#slug').val(new_post_slug);
                    if(data.action === 'create'){
                        let postId = data.post_id;
                        $('#post_id').val(postId);
                    }

                    if(urlsDiv.hasClass('shown-true')){
                        urlsDiv.removeClass('shown-true');
                        urlsDiv.hide(1000);
                    }

                    window.location.hash = new_post_slug;//change #example to #newexample to open new template on page reload
                }
                else showAlertMessage(alert_text_elem, div_alert_elem, 'alert-danger', data.message);
            }
        })
    });

    $('#show_vars').click(function(e){
        let varsDiv = $('#content_vars');
        showHide(varsDiv);
    });

    //------------- - - generate urls - - ----------------
    $('#generate_urls').click(function(){
        let tmpUrl = $(document).find('#ppLink').attr('href');
        let usIt   = tmpUrl.split('/');
        let slug   = usIt[usIt.length - 1];
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'get',
            data: {
                action: 'get_post_content',
                post_slug: slug
            },
            dataType: 'html',
            success: function(data){
                let pp_post_data = JSON.parse(data);
                let markup  = pp_post_data.post_content;
                let domVars = markup.match(/(?<={{\s*).*?(?=\s*}})/gs); //if no variables used,it is null
                let varsData = {};

                if(domVars){
                    for(let i = 0; i < domVars.length; i++){
                        varsData[domVars[i]] = domVars[i];
                    }
                    varsData["url"] = tmpUrl;

                    let dataToApi = {
                        action: 'generate_url',
                        variables: varsData
                    };
                    showHide($('#urls-div'));
                    generateUrls(dataToApi);
                }
                else{
                    $('#getresponse-url').val(tmpUrl);
                    $('#sandlane-url').val(tmpUrl);
                    $('#aweber-url').val(tmpUrl);
                    showHide($('#urls-div'));
                }
            }
        });
    });

    function generateUrls(dataToApi){
        $.ajax({
            url: ajax_object.ajax_url,
            type: 'post',
            data: dataToApi,
            success: function(response){
                let data = JSON.parse(response);
                if(data.success === true){
                    data = data.data;
                    let getResponse = data.getResponse;
                    let sendLane    = data.sendlane;
                    let aweber      = data.aweber;
                    $('#getresponse-url').val(getResponse);
                    $('#sandlane-url').val(sendLane);
                    $('#aweber-url').val(aweber);

                }
                else showAlertMessage(alert_text_elem, div_alert_elem, 'alert-danger', 'At first select template.');
            }
        });
    }

    $('#generate_fb_url').click(function(){
        showHide($('#fb-url-div'));
    });

    appIdInput.bind("paste", function(e){
        let pastedData = e.originalEvent.clipboardData.getData('text');
        let tmpUrl = $(document).find('#ppLink').attr('href');
        $('#facebook-url').val(tmpUrl + '?type=facebook&app-id=' + pastedData);
    });

    appIdInput.bind("keyup", function(e){
        let tmpUrl = $(document).find('#ppLink').attr('href');
        $('#facebook-url').val(tmpUrl + '?type=facebook&app-id=' + $(this).val());
    });

    copyUrlButton.click(function(){
        let urlInput = $(this).closest('.input-group').find('input')[0];
        copyUrl(urlInput);
        $(this).attr('title', 'Copied').tooltip('_fixTitle').tooltip('show');
    });

    copyUrlButton.mouseout(function(){
        $(this).attr('title', 'Click To Copy').tooltip('_fixTitle');
    });
    //------------- - - / generate urls - - ----------------

    //---------------- - - validations - - --------------------
    function validatePageSlug(slug_val){
        let myRe = new RegExp(/^[A-Za-z0-9]+(?:-[A-Za-z0-9]+)*$/, 'g');
        return slug_val.match(myRe);
    }
    //---------------- - - /validations - - --------------------

    //---------------- - - functions - - --------------------

    function initCkEditor(slug, data){
        CKEDITOR.replace(slug + 'editor', {
            width: '100%',
            height: 600,
            allowedContent: true,
            removeButtons: 'Underline,Subscript,Superscript,Copy,Cut,Undo,Redo,Replace,Form,SelectAll,Checkbox,RadioButton,TextField,Textarea,Flash'
        });
        CKEDITOR.instances[slug + 'editor'].setData(data);
    }

    function showAlertMessage(alertDivElem, parentDivElem, alert_class, message) {
        parentDivElem.removeProp('hidden');
        alertDivElem.addClass(alert_class).html(message);
        alertDivElem.show(800);
        setTimeout(function () {
            alertDivElem.hide(800);
        }, 3000);
        setTimeout(function () {
            alertDivElem.removeClass(alert_class);
        }, 3800);
    }

    function showHide(varsDiv){
        if(varsDiv.hasClass('shown-true')){
            varsDiv.removeClass('shown-true');
            varsDiv.hide(1000);
        }
        else{
            varsDiv.addClass('shown-true');
            varsDiv.show(1000);
        }
    }

    function copyUrl(copyText){
        copyText.disabled = false;
        copyText.select();
        copyText.setSelectionRange(0, 99999);
        document.execCommand("copy");
        copyText.disabled = true;
    }

    //---------------- - - /functions - - --------------------


    // ---------------- - - templates - - --------------------
    function getCKEditor(type, addClass = '') {
        return `
        <div id="${type}" class="tab-pane fade show active ${addClass}" role="tabpanel" aria-labelledby="home-tab">
            <form id="${type}_form" >
                <textarea name="${type + 'editor'}"></textarea>
            </form>
        </div>
           
      `;
    }

    function getNavItem(tmp_name, slug) {
        return `
         <li class="mb-20 pl-2"><a class="fs17 d-inline-block font-medium c-blue temp-btn c-blue bgc-lblue template-links" data-slug="${slug}" href="#${slug}">${tmp_name}</a></li>
      `;
    }
    //---------------- - - /templates - - --------------------
});