<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <title>prospect</title>
    <link rel="icon" href=""/>
    <link rel="shortcut icon" href=""/>

    <link rel="stylesheet" href="<?= plugins_url('/../assets/video/css/hover.css', __FILE__) ?>">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video/css/bootstrap.css', __FILE__) ?>">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video/css/style.css', __FILE__) ?>">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video/css/media.css', __FILE__) ?>">


</head>
<body>
<header class="header ">
    <div class="container-fluid wrapper wrapper2">
        <div class="row">
            <div class="col-90 d-flex align-items-center justify-content-start ">
                <p class="fs35 c-black font-light mb-0 text-center header-title">
                    <span class="font-semi-bold mr-3">Milan:</span>Here's a personalised message for you and the team and<a href="javascript:void(0)" class="header-link ml-2 font-semi-bold">NOVP</a>
                </p>
            </div>
        </div>
    </div>
</header>
<main>
    <section class="frameworks-block">
        <div class="container-fluid wrapper1 ">
            <div class="row mb-sm-4">
                <div class="col-sm-6 col-12 mb-sm-0 mb-4">
                    <h1 class="mb-0 text-sm-left text-center">
                        <a href="#" class="d-inline-block logo">
                            <img src="<?= plugins_url('prospectp/assets/video/images/logo.png') ?>" alt="logo">
                        </a>
                    </h1>

                </div>
                <div class="col-sm-6 col-12 d-flex align-items-center justify-content-sm-end justify-content-center ">
                        <a href="#" class="pink-btn d-inline-block c-white fs21 font-semi-bold mb-3 mb-sm-0"><?=$template_variables['call_to_action1']?></a>
                </div>
            </div>
        </div>
        <div class="video-box">
            <div class="container-fluid wrapper wrapper2">
                <div class="row">
                    <div class="col-12">
                        <iframe src="https://www.youtube.com/embed/<?=$template_variables['video_path']?>" class="video-element">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>

        <div class="container-fluid wrapper wrapper2">
            <div class="row">
                <div class="col-12 ">
                    <div class="txt-block">
                        <h2 class="fs50 font-light c-black txt-block_title"><?=$template_variables['heading1']?></h2>
                        <p class="fs23 font-light c-black txt-block_txt"><?=$template_variables['sub_heading1']?></p>
                        <a href="#" class= "pink-btn d-inline-block c-white fs21 font-semi-bold"><?=$template_variables['call_to_action2']?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blue-block">
        <div class="container-fluid wrapper wrapper2 ">
            <div class="row">
                <div class="col-lg-6 col-12 pl-72 ">
                    <div class="d-flex">
                        <div class=" right-block">
                            <h2 class="fs50 font-light c-white blue-block_title mw-420"><?=$template_variables['heading2']?></h2>
                            <p class="fs23 font-light c-white blue-block_txt mw-420"><?=$template_variables['sub_heading2_1']?></p>
                            <p class="fs23 font-light c-white blue-block_txt mw-420"><?=$template_variables['sub_heading2_2']?></p>
                            <div class="d-md-none d-block">
                                <div class="project-box">
                                    <div class="project-box_txt">
                                        <h3 class="fs26 c-black font-semi-bold">Projects</h3>
                                        <p class="fs21 c-grey mb-0">Project management software that lets you own the big picture.</p>
                                    </div>
                                </div>
                                <div class="project-box">
                                    <div class="project-box_txt">
                                        <h3 class="fs26 c-black font-semi-bold">Projects</h3>
                                        <p class="fs21 c-grey mb-0">Project management software that lets you own the big picture.</p>
                                    </div>
                                </div>
                            </div>
                            <div>
                                <a href="#" class="pink-btn d-inline-block c-white fs21 font-semi-bold mb-3"><?=$template_variables['call_to_action3']?></a>
                                <p class="fs18 c-white pl-2">or contact our <a href="#" class="right-block_link c-white">sales team</a></p>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-lg-6 col-12  d-md-block d-none ">
                    <div class="d-flex justify-content-lg-end justify-content-start">
                        <div>
                            <div class="mw-420 left-block">
                                <h2 class="fs50 font-light c-white blue-block_title "><?=$template_variables['heading3']?></h2>
                                <p class="fs23 font-light c-white blue-block_txt"><?=$template_variables['sub_heading3']?></p>
                            </div>
                            <div class="project-box">
                                <div class="project-box_txt">
                                    <h3 class="fs26 c-black font-semi-bold"><?=$template_variables['project1']?></h3>
                                    <p class="fs21 c-grey mb-0"><?=$template_variables['description1']?></p>
                                </div>
                            </div>
                            <div class="project-box">
                                <div class="project-box_txt">
                                    <h3 class="fs26 c-black font-semi-bold"><?=$template_variables['project2']?></h3>
                                    <p class="fs21 c-grey mb-0"><?=$template_variables['description2']?></p>
                                </div>
                            </div>
                            <div class="project-box">
                                <div class="project-box_txt">
                                    <h3 class="fs26 c-black font-semi-bold"><?=$template_variables['project3']?></h3>
                                    <p class="fs21 c-grey mb-0"><?=$template_variables['description3']?></p>
                                </div>
                            </div>
                            </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
</main>

<script src="<?= plugins_url('/../assets/video/js/jquery-3.4.1.min.js', __FILE__) ?>"></script>
<script src="<?= plugins_url('/../assets/video/js/bootstrap.js', __FILE__) ?>"></script>
<script src="<?= plugins_url('/../assets/video/js/main.js', __FILE__) ?>"></script>
</body>
</html>
