<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=yes"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <meta property="og:image" content=""/>
    <meta property="og:title" content=""/>
    <meta property="og:description" content=""/>
    <title>prospect</title>
    <link rel="icon" href=""/>
    <link rel="shortcut icon" href=""/>

    <link rel="stylesheet" href="<?= plugins_url('/../assets/video-text/css/hover.css', __FILE__) ?>">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro&display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video-text/css/bootstrap.css', __FILE__) ?>">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video-text/css/style.css', __FILE__) ?>">
    <link rel="stylesheet" href="<?= plugins_url('/../assets/video/css/media.css', __FILE__) ?>">


</head>
<body>
<header class="header">
    <div class="container-fluid wrapper wrapper2">
        <div class="row">
            <div class="col-90 d-flex align-items-center justify-content-start ">
                <p class="fs35 c-white font-light mb-0 text-center header-title">
                    <span class="font-semi-bold mr-3 ">Milan:</span>Here's a personalised message for you and the team and<a href="javascript:void(0)" class="header-link ml-2 font-semi-bold">NOVP</a>
                </p>
            </div>
        </div>
    </div>
</header>
<main>
    <section class="frameworks-block">
        <div class="container-fluid wrapper1 mb-md-0 mb-4">
            <div class="row">
                <div class="col-sm-6 col-12 mb-sm-0 mb-4">
                    <h1 class="mb-0 text-sm-left text-center">
                        <a href="#" class="d-inline-block logo">
                            <img src="<?= plugins_url('prospectp/assets/video-text/images/logo.png') ?>" alt="logo">
                        </a>
                    </h1>
                </div>
                <div class="col-sm-6 col-12 d-flex align-items-center justify-content-sm-end justify-content-center ">
                    <a href="javascript:void(0)" class="blue-btn d-inline-block c-white fs21 font-semi-bold mb-3 mb-sm-0 call-to-action editable" id="call_to_action1"><?=$template_variables['call_to_action2']?></a>
                </div>
            </div>
        </div>
        <div class="container-fluid wrapper wrapper2 mb-17">
            <div class="row">
                <div class="col-12 ">
                    <div class="txt-block">
                        <div>
                            <h2 class="fs50 font-light c-black txt-block_title black-border large-editable" id="heading1"><?=$template_variables['heading1']?></h2>
                        </div>
                        <div>
                            <p class="fs23 font-light c-black txt-block_txt black-border large-editable" id="sub_heading1"><?=$template_variables['sub_heading1']?></p>
                        </div>
                            <a href="javascript:void(0)" class= "blue-btn d-inline-block c-white fs21 font-semi-bold editable" id="call_to_action2"><?=$template_variables['call_to_action2']?></a>
                    </div>
                </div>
            </div>
        </div>
        <div class="video-box">
            <div class="container-fluid wrapper wrapper2">
                <div class="row">
                    <div class="col-12">
                        <iframe src="https://www.youtube.com/embed/<?=$template_variables['video_path']?>" class="video-element" id="video_path">
                        </iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="purple-block">
        <div class="container-fluid wrapper wrapper2 ">
            <div class="row mb-115">
                <div class="col-md-8 col-12 mb-md-0 mb-5">
                    <h2 class="fs50 c-white font-light purple-block_title large-editable" id="heading2"><?=$template_variables['heading2']?></h2>
                    <p class="c-white font-light fs23 purple-block_txt mb-0 mw-595 large-editable" id="sub_heading2"><?=$template_variables['sub_heading2']?></p>
                </div>
                <div class="col-md-4 col-12 d-flex justify-content-md-end justify-content-start">
                    <div class="getintouch">
                        <a href="javascript:void(0)" class="purple-btn d-inline-block c-white fs21 font-semi-bold mb-md-3 mb-2 editable" id="call_to_action3"><?=$template_variables['call_to_action3']?></a>
                        <p class="fs18 c-white pl-2 c-white">or contact our <a href="javascript:void(0)" class="right-block_link c-white">sales team</a></p>
                    </div>
                </div>
            </div>
            <div class="row ">
                <div class="col-md-6 col-12">
                    <div class="project-box">
                        <div class="project-box_txt">
                            <h3 class="fs26 c-black font-semi-bold mb-3"><?=$template_variables['project1']?></h3>
                            <p class="fs21 c-grey mb-35"><?=$template_variables['description1']?></p>
                            <a href="#" class="c-purple font-semi-bold fs21 purple-border">Some link text</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12">
                    <div class="project-box">
                        <div class="project-box_txt">
                            <h3 class="fs26 c-black font-semi-bold mb-3"><?=$template_variables['project2']?></h3>
                            <p class="fs21 c-grey mb-35"><?=$template_variables['description2']?></p>
                            <a href="#" class="c-purple font-semi-bold fs21 purple-border">Some link text</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 d-md-block d-none">
                    <div class="project-box">
                        <div class="project-box_txt">
                            <h3 class="fs26 c-black font-semi-bold mb-3"><?=$template_variables['project3']?></h3>
                            <p class="fs21 c-grey mb-35"><?=$template_variables['description3']?></p>
                            <a href="#" class="c-purple font-semi-bold fs21 purple-border">Some link text</a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-12 d-md-block d-none">
                    <div class="project-box">
                        <div class="project-box_txt">
                            <h3 class="fs26 c-black font-semi-bold mb-3"><?=$template_variables['project4']?></h3>
                            <p class="fs21 c-grey mb-35"><?=$template_variables['description4']?></p>
                            <a href="#" class="c-purple font-semi-bold fs21 purple-border">Some link text</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="client-block">
                <div class="row">

                    <?php if(!empty($template_variables['clients_data'])){ ?>
                        <div class="col-12">
                            <h2 class="fs30 c-white font-light client-block_title ">What clients say:</h2>
                        </div>
                        <?php for($i = 0; $i < count($template_variables['clients_data']['client_names']); $i++){?>

                            <div class="col-lg-3 col-sm-6 col-12 trans">
                                <div class="client-box text-center trans">
                                    <div class="img-box text-center">
                                        <span class="client-box_img mb-3" style="background-position: center; background-image: url(<?= plugins_url('prospectp/assets/video-text/images/'.$template_variables['clients_data']['images'][$i]) ?>)"></span>
                                        <h2 class="fs20 c-black font-medium img-box_title"><?=$template_variables['clients_data']['client_names'][$i]?></h2>
                                    </div>
                                    <div class="client-txt_box">
                                        <p class="client-box_txt fs20">
                                            <?=$template_variables['clients_data']['reviews'][$i]?>
                                        </p>
                                        <span><img src="<?= plugins_url('prospectp/assets/video-text/images/star.png') ?>" alt="star"></span>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="contact-block">
            <div class="container-fluid wrapper wrapper2">
                <div class="row">
                    <div class="col-md-8 col-12 mb-md-0 mb-5">
                        <h2 class="fs50 c-black font-light purple-block_title"><?=$template_variables['contact_us']?></h2>
                        <p class="c-black font-light fs23 purple-block_txt mw-595 mb-0"><?=$template_variables['contact_us_text']?></p>
                    </div>
                    <div class="col-md-4 col-12 d-flex justify-content-md-end justify-content-start">
                        <div>
                            <a href="#" class="purple-btn d-inline-block c-white fs21 font-semi-bold mb-md-3 mb-2"><?=$template_variables['call_to_action4']?></a>
                            <p class="fs18 pl-2 c-black">or contact our <a href="#" class="right-block_link c-black">sales team</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="map-block">
            <img src="<?= plugins_url('prospectp/assets/video-text/images/map.png') ?>" alt="map">
        </div>
    </section>
</main>

<script src="<?= plugins_url('/../assets/video-text/js/jquery-3.4.1.min.js', __FILE__) ?>"></script>
<script src="<?= plugins_url('/../assets/video-text/js/bootstrap.js', __FILE__) ?>"></script>
<script src="<?= plugins_url('/../assets/video-text/js/main.js', __FILE__) ?>"></script>
</body>
</html>
