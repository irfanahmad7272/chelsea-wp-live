<header>
    <div class="row">
        <div class="col-12 p-3 text-right">
            <a href="javascript:void(0)" class="fs17 c-black font-medium pl-2"><b>General Settings</b></a>
        </div>
    </div>
</header>
<div class="row">
    <div class="col-20">
        <div class="left-part h-100">
            <div>
                <ul class="left-list pb-4 mb-0">
                    <li><p class="fs14 c-grey font-medium mb-20">Templates</p></li>
                    <li><a href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect'?>" class="fs17 c-black font-medium pl-2"><b>Templates</b></a></li>
                </ul>

                <ul class="bottom-list">
                    <li class="mb-20"><p href="#" class="fs14 c-grey font-medium ">Templates</p></li>
                    <?php foreach($userPpPosts as $post){ ?>
                        <li class="mb-20 pl-2"><a class="fs17 d-inline-block font-medium c-black template-links" data-slug="<?=$post->post_name?>"
                                                  href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect' . '#' .$post->post_name?>"><?=$post->post_title?></a></li>
                    <?php }?>
                </ul>
                <p class="mb-20 pl-4"><a href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect' . '#' . 'new_template'?>" class="fs17 d-inline-block font-medium temp-btn bgc-blue" >New Template</a></p>
            </div>
            <div class="pl-4">
                <a href="<?=get_site_url() . '/wp-admin/admin.php?page=pp_reports'?>" class="fs17 d-inline-block font-medium report-btn bgc-red mb-20 position-relative"><i class="fas fa-file-alt doc-icon"></i> Reporting</a>
            </div>
        </div>
    </div>
    <div class="col-80">
        <div class="right-part h-100 content-div">
            <div class="setting-main">
                <div class="row">
                    <div class="col-xl-7 col-12 mb-4 col-custum-7">
                        <div class="setting-main_right">
                            <div class="setting-right_head">
                                <a target="_blank" href="https://zapier.com/developer/public-invite/76350/4ff6706099f0e021daae97e35f663391/" class="update-link">Open Zapier App</a>
                            </div>

                            <div class="setting-right_head">
                                <p class="setting-title">Billing & Settings</p>
                            </div>
                            <div class="billing-settings-box">
                                <div class="bil-set-block">
                                    <h3 class="label">Licence</h3>
                                    <div class="bil-set_box bgc-lblue">
                                        <?php if (isset($_SESSION['plan_error'])) { ?>
                                            <div class="alert alert-danger">
                                                <?= $_SESSION['plan_error'] ?>
                                            </div>
                                        <?php unset($_SESSION["plan_error"]);} ?>

                                        <?php if (isset($_SESSION['plan_alert'])) { ?>
                                            <div class="alert alert-success">
                                                <?= $_SESSION['plan_alert'] ?>
                                            </div>
                                        <?php unset($_SESSION["plan_alert"]);} ?>

                                        <p class="bil-set_box-title">Your Licence: <span class="c-black font-medium">Up to <?= $subPlan['sites']?> <?=$subPlan['sites'] > 1 || $subPlan['sites'] === 'unlimited' ? ' sites' : ' site'?></span></p>

                                        <?php if($subPlan['name'] === 'basic') { ?>
                                        <div class="d-flex flex-wrap">
                                            <a href="#" class="btn btn-grade up-btn change-plan" data-plan-id="3">Upgrade to Unlimited</a>
                                            <a href="#" class="btn btn-grade up-btn change-plan" data-plan-id="2">Upgrade to 5 sites</a>
                                        </div>
                                        <?php }elseif($subPlan['name'] === 'pro'){?>

                                        <div class="d-flex flex-wrap">
                                            <a href="#" class="btn btn-grade up-btn change-plan" data-plan-id="3">Upgrade to Unlimited</a>
                                            <a href="#" class="btn btn-grade down-btn change-plan" data-plan-id="1">Downgrade to Single site</a>
                                        </div>
                                        <?php }elseif($subPlan['name'] === 'premium'){?>

                                        <div class="d-flex flex-wrap">
                                            <a href="#" class="btn btn-grade down-btn change-plan" data-plan-id="2">Downgrade to 5 sites</a>
                                            <a href="#" class="btn btn-grade down-btn change-plan ml-2" data-plan-id="1">Downgrade to Single site</a>
                                        </div>
                                        <?php }?>
<!--                                        <div class="d-flex">-->
<!--                                            <p class="txt-expire">Expire: <span class="c-black font-medium">25/6/2020</span></p>-->
<!--                                            <a href="#" class="bil-set_link">Extend</a>-->
<!--                                        </div>-->

                                        <form method="post" id="form-sub-plan" action="<?php echo esc_url(admin_url('admin-post.php')); ?>" hidden>
                                            <input type="hidden" name="action" value="update_sub_plan">
                                            <input type="hidden" id="sub-plan-id" name="plan_id" value="">
                                        </form>

                                    </div>
                                </div>

                                <div class="bil-set-block">
                                    <h3 class="label">Sites</h3>

                                    <div class="bil-set_box ">

                                        <div class="row div_alert"  hidden style="margin-top: 15px;">
                                            <div class="col-md-12">
                                                <div class="alert alert_text" style="display: none;"></div>
                                            </div>
                                        </div>

                                        <p class="bil-set_box-title">Your sites: <span class="c-black font-medium"> <span id="total_sites"><?=count($userSitesData['sites'])?></span> <?= ' of ' . $subPlan['sites']?> </span></p>

                                        <div class="form-group position-relative">
                                            <label for="current_site" class="">Current Site</label>
                                            <input type="text" class="form-control revoke-input" id="current_site" value="<?= $_SERVER['HTTP_HOST'] ?>" disabled>
                                        </div>

                                        <?php if(count($userSitesData['sites']) > 1){ ?>
                                            <label for="">Other Sites</label>
                                            <?php foreach($userSitesData['sites'] as $site){ if($site['domain'] !== $_SERVER['HTTP_HOST']) {?>
                                            <div class="form-group position-relative site-div">
                                                <input type="hidden" class="site-id" value="<?= $site['id'] ?>">
                                                <label for="revoke" class="input-label-link revoke-label">Revoke</label>
                                                <input type="text" class="form-control revoke-input" value="<?= $site['domain'] ?>" disabled id="revoke" autocomplete="off">
                                            </div>
                                            <?php }} ?>
                                            <div class="form-group position-relative" hidden>
                                                <label for="password1" class="input-label">Password:</label>
                                                <input type="password" class="form-control password-input" id="password1" autocomplete="off">
                                                <a href="#" class="confirm-link pwd-confirm" data-site-id="">Confirm</a>
                                                <a href="#" class="cancel-link">Cancel</a>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="bil-set-block">
                                    <h3 class="label">Registered Email</h3>
                                    <div class="bil-set_box ">
                                        <div class="form-group position-relative">
                                            <form method="post" action="<?php echo esc_url(admin_url('admin-post.php')); ?>" id="email-form">
                                                <input type="hidden" name="action" value="update_email">
                                                <div class="d-flex justify-content-between mb-1">
                                                    <label for="email1" class="label mb-0">Email</label>
                                                    <a href="#" id="email-submit" class="update-link">Update</a>
                                                </div>
                                                <input type="email" name="email" class="form-control email-input"
                                                       id="email1"
                                                       value="<?= isset($userData) && $userData ? $userData['email'] : '' ?>">
                                            </form>
                                        </div>
                                    </div>
                                </div>

                                <div class="bil-set-block">
                                    <h3 class="label">Change Password</h3>
                                    <div class="bil-set_box ">
                                        <form method="post" action="<?php echo esc_url(admin_url('admin-post.php')); ?>" id="pwd_form">
                                            <?php if (isset($_SESSION['password_errors'])) { ?>
                                                <?php if (gettype($_SESSION['password_errors']) === 'string') { ?>
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            <li><?= $_SESSION['password_errors'] ?></li>
                                                        </ul>
                                                    </div>
                                                <?php } else { ?>
                                                    <div class="alert alert-danger">
                                                        <ul>
                                                            <?php foreach ($_SESSION['password_errors'] as $error) { ?>
                                                                <li><?= $error[0] ?></li>
                                                            <?php } ?>
                                                        </ul>
                                                    </div>
                                                <?php } ?>
                                            <?php unset($_SESSION["password_errors"]);} ?>

                                            <?php if (isset($_SESSION['alert'])) { ?>
                                                <div class="alert alert-success">
                                                    <?= $_SESSION['alert'] ?>
                                                </div>
                                            <?php unset($_SESSION["alert"]);} ?>
                                            <input type="hidden" name="action" value="update_password">
                                            <div class="form-group position-relative">
                                                <div class="d-flex justify-content-between mb-1">
                                                    <label for="password2" class="label mb-0">Old Password</label>
                                                    <a href="#" class="update-link" id="pwd_submit">Update</a>
                                                </div>
                                                <input type="password" name="old_password" class="form-control email-input" id="password2">
                                            </div>
                                            <div class="form-group position-relative">
                                                <label for="password3" class="label mb-0">New Password</label>
                                                <input type="password" name="new_password" class="form-control email-input" id="password3">
                                            </div>
                                        </form>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</div>

<?php
$array = [
    'url'     => 'prospect.loc/pp/slug',
    'name'    => 'Valod',
    'email'   => 'valod@valod.com',
    'company' => 'Valodi Company'
];
