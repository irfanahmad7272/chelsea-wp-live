<div class="login-page">
    <div class="login-box">
        <p class="login-box__text">Type your email to receive reset link</p>
        <form method="POST" action="<?php echo esc_url(admin_url('admin-post.php')); ?>">
            <?php if (isset($_SESSION['errors'])) { ?>
                <?php if (gettype($_SESSION['errors']) === 'string') { ?>
                    <div class="alert alert-danger">
                        <ul>
                            <li><?= $_SESSION['errors'] ?></li>
                        </ul>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php foreach ($_SESSION['errors'] as $error) { ?>
                                <li><?= $error[0] ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <?php unset($_SESSION["errors"]);
            } ?>

            <?php if (isset($_SESSION['alert'])) { ?>
                <div class="alert alert-success">
                    <?= $_SESSION['alert'] ?>
                </div>
            <?php unset($_SESSION["alert"]);} ?>
            <input type="hidden" name="action" value="reset_form_api">
            <div class="form-group">
                <label class="login-box__label d-block text-left" id="email-input">Email</label>
                <input type="text" name="email" class="form-control">
            </div>

            <div class="login-box__btn">
                <button type="submit" class="btn btn-signIn w240">SEND EMAIL</button>
            </div>
        </form>
    </div>
</div>

