<header>
    <div class="row">
        <div class="col-12 p-3 text-right">
            <a href="<?=get_site_url() . '/wp-admin/admin.php?page=pp_settings'?>" class="fs17 c-black font-medium pl-2"><b>General Settings</b></a>
        </div>
    </div>
    <input type="hidden" id="http-host" value="<?= $_SERVER['HTTP_HOST'] ?>">
    <input type="hidden" id=protocol" value="<?= (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) ? "https://" : "http://" ?>">
</header>
    <div class="row">
        <div class="col-20">
            <div class="left-part h-100">
                <ul class="left-list pb-4 mb-0">
                    <li><p class="fs14 c-grey font-medium mb-20">Templates</p></li>
                    <li><a href="javascript:void(0)" class="fs17 c-black font-medium pl-2"><b>Templates</b></a></li>
                </ul>

                <div class="custom-div">
                    <ul class="bottom-list">
                        <li class="mb-20"><p href="#" class="fs14 c-grey font-medium list-header">Templates</p></li>
                        <?php foreach($userPpPosts as $post){ ?>
                            <li class="mb-20 pl-2"><a class="fs17 d-inline-block font-medium c-black template-links" data-slug="<?=$post->post_name?>" href="javascript:void(0)"><?=$post->post_title?></a></li>
                        <?php }?>
                    </ul>
                    <p class="mb-20 pl-4" style="margin-bottom: 75px;"><a href="#" class="fs17 d-inline-block font-medium c-white temp-btn bgc-blue new_tmp_button" data-toggle="modal" data-target="#templatesModal">New Template</a></p>
                </div>
                <div class="pl-4 reporting-div">
                    <a href="<?=get_site_url() . '/wp-admin/admin.php?page=pp_reports'?>" class="fs17 d-inline-block font-medium report-btn bgc-red mb-20 position-relative"><i class="fas fa-file-alt doc-icon"></i> Reporting</a>
                </div>
            </div>
        </div>
        <div class="col-80">
            <div class="row div_alert"  hidden style="margin-top: 15px;">
                <div class="col-md-12">
                    <div class="alert alert_text" style="display: none;"></div>
                </div>
            </div>

            <div class="right-part h-100 content-div">
                <div class="row">

                    <div class="col-6">
                        <h2 class="right-title fs17 font-medium c-blue">Edit Template</h2>
                        <form id="template_form" data-id="">
                            <input type="hidden" id="post_id" value="">
                            <input type="hidden" id="tmp_type" value="">
                            <div class="mb-3">
                                <label for="title" class="fs16 c-grey d-block">Name</label>
                                <input id="title" type="text" placeholder="My Template 2">
                            </div>
<!--                            <div style="display: flex; align-items: flex-end; justify-content: space-between;">-->
                            <div>
                                <div>
                                    <label for="slug" class="fs16 c-grey d-block">URL</label>
                                    <div class="input-group form-group">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text span-domain"><?= $_SERVER['HTTP_HOST'] ?>/pp/</span>
                                        </div>
                                        <input type="text" id="slug" placeholder="My Template 2">
                                    </div>
                                </div>
                                <div>
                                    <button type="button" class="btn btn-success" id="update_template" data-id="">Save</button>
                                </div>
                            </div>
                        </form>
                    </div>

                    <div class="col-6">
                        <a href="javascript:void(0)" id="generate_urls" class="open-txt fs15 font-medium c-red">Generate Autoresponder URLs</a>
                        <div id="urls-div" style="display: none;">
                            <div class="mt-3">
                                <label for="getresponse-url" class="fs16 c-grey d-block">Get Response URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text span-domain cursor-pointer copy-url" data-toggle="tooltip" data-placement="bottom" title="Click To Copy"><a href="javascript:void(0)">Copy Url</a></span>
                                    </div>
                                    <input id="getresponse-url"  class="w-75 custom-radius" type="text" placeholder="" value="" disabled>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="sandlane-url" class="fs16 c-grey d-block">Sendlane URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text span-domain cursor-pointer copy-url" data-toggle="tooltip" data-placement="bottom" title="Click To Copy"><a href="javascript:void(0)">Copy Url</a></span>
                                    </div>
                                    <input id="sandlane-url"  class="w-75 custom-radius" type="text" placeholder="" value="" disabled>
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="aweber-url" class="fs16 c-grey d-block">AWeber URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text span-domain cursor-pointer copy-url" data-toggle="tooltip" data-placement="bottom" title="Click To Copy"><a href="javascript:void(0)">Copy Url</a></span>
                                    </div>
                                    <input id="aweber-url"  class="w-75 custom-radius" type="text" placeholder="" value="" disabled>
                                </div>
                            </div>
                        </div>

                        <div class="mt-3">
                            <a href="javascript:void(0)" id="generate_fb_url" class="open-txt fs15 font-medium c-red mt-3">Generate Facebook URL</a>
                        </div>

                        <div id="fb-url-div" style="display: none;">
                            <div class="mt-3">
                                <button class="btn btn-info" data-toggle="modal" data-target="#guideModal">Show guide</button>
                            </div>
                            <div class="mt-3">
                                <label for="app-id" class="fs16 c-grey d-block">Facebook App ID</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span style="padding-right: 25px;" class="input-group-text span-domain cursor-pointer">App ID</span>
                                    </div>
                                    <input id="app-id" style="border-color: rgb(221, 223, 225);"  class="w-75 custom-radius" type="text" placeholder="" value="">
                                </div>
                            </div>
                            <div class="mt-3">
                                <label for="facebook-url" class="fs16 c-grey d-block">Facebook URL</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text span-domain cursor-pointer copy-url" data-toggle="tooltip" data-placement="bottom" title="Click To Copy"><a href="javascript:void(0)">Copy Url</a></span>
                                    </div>
                                    <input id="facebook-url"  class="w-75 custom-radius" type="text" placeholder="" value="" disabled>
                                </div>
                            </div>
                            <div class="mt-3 w-75">
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <div class="text-center">
                                            <div style="color: #fa4b56;">*Without valid app id facebook link will not work in your custom tab. Paste it to App Id field and link will be generated automatically.</div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div class="btn-block d-flex justify-content-end">
                    <ul class="d-flex justify-content-between w-100">
                        <li class="mt-2"><a href="javascript:void(0)" id="show_vars" class="open-txt fs15 font-medium c-red">Show Content Variables</a></li>
                        <li class="mt-2"><a href="#" target="_blank" id="ppLink" class="open-txt fs15 font-medium c-red">Open in New Tab</a></li>
                    </ul>
                </div>

                <div id="content_vars" class="mt-4" style="display: none;">
                    <div class="row">
                        <div class="mb-3 col-3">
                            <label for="first-name" class="fs16 c-grey d-block">First Name</label>
                            <input id="first-name"  type="text" placeholder="" value="{{first_name}}" disabled>
                        </div>
                        <div class="col-3">
                            <label for="last-name" class="fs16 c-grey d-block">Last Name</label>
                            <input id="last-name"  type="text" placeholder="" value="{{last_name}}" disabled>
                        </div>
                        <div class="col-3">
                            <label for="full-name" class="fs16 c-grey d-block">Full Name</label>
                            <input id="full-name"  type="text" placeholder="" value="{{full_name}}" disabled>
                        </div>
                    </div>

                    <div class="row">
                        <div class="mb-3 col-3">
                            <label for="company" class="fs16 c-grey d-block">Company <sup style="color: #fa4b56;">**</sup></label>
                            <input id="company"  type="text" placeholder="" value="{{company}}" disabled>
                        </div>
                    </div>

                    <div class="row text-center mb-4">
                        <div class="col" style="color: #fa4b56;"><sup>*</sup> Paste variables to editor content.</div>
                        <div class="col" style="color: #fa4b56;"><sup>**</sup> Company is not supported in templates for Facebook</div>
                    </div>
                </div>

            </div>
        </div>

    </div>


<!-- Modal -->
<div class="modal fade px-3" id="templatesModal" tabindex="-1" role="dialog" aria-labelledby="templatesModalLabel"
     aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class=" modal-content h-100 border-0">
            <div class="modal-body position-relative">
                <div class="row ">
                    <div class="col-12 position-static">
                        <div class="modal-part ">
                            <a href="#" class="close-btn">
                                <i class="fas fa-times" data-dismiss="modal"></i>
                            </a>
                            <h2 class="modal-title fs17 font-medium c-blue">Create New Template</h2>
                            <form action="">
                                <div class="mb-3">
                                    <label for="name" class="fs16 c-grey d-block mb-1">Name</label>
                                    <input id="template_name" type="text" placeholder="My Template 2">
                                </div>
                                <div>
                                    <label for="name" class="fs16 c-grey d-block mb-1">URL</label>
                                    <input id="template_slug" type="text" placeholder="example.com/jack">
                                </div>
                            </form>
                        </div>
                        <h2 class="template-text fs17 font-medium c-blue mb-3">Select Template</h2>
                    </div>
                    <div class="col-sm-4 pr-2 template-box">
                        <a href="javascript:void(0)" data-type="video-text" class="template-link img-box d-inline-block mb-3 template-types active">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-3-video-text.jpg') ?>" alt="">
                            <span class="img-box_text fs14 font-medium"><i class="fas fa-check-circle"></i> Template 1</span>
                        </a>
                        ​
                    </div>
                    <div class="col-sm-4 pl-2 pr-2 template-box">
                        <a href="javascript:void(0)" data-type="video" class="template-link img-box d-inline-block mb-3 template-types">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-1-video.jpg') ?>" alt="" >
                            <span class="img-box_text fs14 font-medium "><i class="fas fa-check-circle"></i> Template 2</span>
                        </a>
                    </div>
                    <div class="col-sm-4 pl-2 template-box">
                        <a href="javascript:void(0)" data-type="text" class="template-link img-box d-inline-block mb-3 template-types">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-2-text.jpg') ?>" alt="">
                            <span class="img-box_text fs14 font-medium"><i class="fas fa-check-circle"></i> Template 3</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
                <a href="#" data-dismiss="modal" class="btn btn-create" id="select_template">CREATE TEMPLATE</a>
            </div>
        </div>
    </div>
</div>


<!-- Fb app id guide Modal -->
<div class="modal fade" id="guideModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Modal title</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>- <a target="_blank" href="https://developers.facebook.com/docs/facebook-login/">Login</a> to developer facebook.</p>
                <p>- From the top select My Apps->Create App.</p>
                <p>- Go to Settings->Basic.</p>
                <p>- At the bottom click on <i>+ Add Platform</i> and select Page Tab.</p>
                <p>- Copy Facebook URL you created here and paste it to <i> Secure Page Tab URL.</i></p>
                <p>- <b>Save Changes.</b></p>
                <p>- At the top of the same page find <i>Privacy Policy URL.</i></p>
                <p>- Paste there your webside privacy policy URL and <b>Save Changes.</b></p>
                <p>- After it you will be able to <b>switch development mode on</b> at the top of the same page(<i>Status: In Development</i>).</p>
                <p>- Copy <b>APP ID:</b> and go to Prospect Plugin Templates page and click on <b>"Generate Facebook URL"</b></p>
                <p>- Paste APP ID in the blank field and facebook url will be generated automatically.</p>
                <p>- Here is the link that will connect the App you already created with your Facebook custom tab.</p>
                <p>http://www.facebook.com/dialog/pagetab?app_id=YOUR_APP_ID&next=YOUR_URL</p>
                <p>- In this URL change <b>YOUR_APP_ID</b> with your App Id.</p>
                <p>- And change <b>YOUR_URL</b> with the Facebook URL you created in Templates page.</p>
                <p>- Go to this link http://www.facebook.com/dialog/pagetab?app_id=YOUR_APP_ID&next=YOUR_URL with your custom YOUR_APP_ID and YOUR_URL and connect App with your Fb custom tab.</p>
                <p>- Go to your Fb page, click on custom tab and enjoy.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- /Fb app id guide Modal -->