<div id="template-page" class="container-fluid">

    <nav class="tabs mt-3" style="z-index: 10;">
        <ul class="-primary nav nav-tabs parent-navs">
            <?php foreach($userPpPosts as $post){ ?>
                <li class="nav-item">
                    <a class="nav-link pp-post" data-slug="<?=$post->post_name?>" href="#<?=$post->post_name?>"><?=$post->post_title?></a>
                </li>
            <?php }?>
        </ul>
        <ul class="nav nav-tabs parent-navs">
            <li class="nav-item" id="new_li" style="background-color: #72777c94;">
                <a class="nav-link" data-toggle="modal" data-target="#templatesModal" href="#">+ New</a>
            </li>
        </ul>
    </nav>

    <div class="tab-content mt-3" id="tab_content">

        <div class="tab-pane fade show active" id="" role="tabpanel" aria-labelledby="home-tab">
        </div>

    </div>
</div>

<!-- Modal -->
<div class="modal fade px-3" id="templatesModal" tabindex="-1" role="dialog" aria-labelledby="templatesModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class=" modal-content h-100 border-0">
            <div class="modal-body position-relative">
                <div class="row ">
                    <div class="col-12 position-static">
                        <div class="modal-part ">
                            <a href="#" class="close-btn">
                                <i class="fas fa-times"></i>
                            </a>
                            <h2 class="modal-title fs17 font-medium c-blue">Create New Template</h2>
                            <form action="">
                                <div class="mb-3">
                                    <label for="name" class="fs16 c-grey d-block mb-1">Name</label>
                                    <input id="name" type="text" placeholder="My Template 2">
                                </div>
                                <div>
                                    <label for="name" class="fs16 c-grey d-block mb-1">URL</label>
                                    <input id="name" type="text" placeholder="example.com/jack">
                                </div>
                            </form>
                        </div>
                        <h2 class="template-text fs17 font-medium c-blue mb-3">Select Template</h2>
                    </div>
                    <div class="col-sm-4 pr-2 template-box">
                        <a href="#" class="template-link img-box d-inline-block mb-3 active">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-3-video-text.jpg') ?>"
                                 alt="">
                            <span class="img-box_text fs14 font-medium"><i
                                        class="fas fa-check-circle"></i> Template 1</span>
                        </a>

                    </div>
                    <div class="col-sm-4 pl-2 pr-2 template-box">
                        <a href="#" class="template-link img-box d-inline-block mb-3">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-1-video.jpg') ?>" alt="">
                            <span class="img-box_text fs14 font-medium "><i class="fas fa-check-circle"></i> Template 2</span>
                        </a>
                    </div>
                    <div class="col-sm-4 pl-2 template-box">
                        <a href="#" class="template-link img-box d-inline-block mb-3">
                            <img src="<?php echo plugins_url('prospectp/assets/image/template-2-text.jpg') ?>" alt="">
                            <span class="img-box_text fs14 font-medium"><i
                                        class="fas fa-check-circle"></i> Template 3</span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="modal-footer border-0">
                <a href="#" class="btn btn-create">CREATE TEMPLATE</a>
            </div>
        </div>
    </div>
</div>


