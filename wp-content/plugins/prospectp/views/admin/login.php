<div class="login-page">
    <div class="login-box">
        <div class="login-box__logo">
<!--            <img src="--><?php //echo plugins_url('prospectp/assets/image/logo-grocano.png') ?><!--" class="image-auto"-->
<!--                 alt="Logo">-->
        </div>
        <p class="login-box__text">Please login to continue</p>
        <form method="POST" action="<?php echo esc_url(admin_url('admin-post.php')); ?>">
            <?php if (isset($_SESSION['errors'])) { ?>
                <?php if (gettype($_SESSION['errors']) === 'string') { ?>
                    <div class="alert alert-danger">
                        <ul>
                            <li><?= $_SESSION['errors'] ?></li>
                        </ul>
                    </div>
                <?php } else { ?>
                    <div class="alert alert-danger">
                        <ul>
                            <?php foreach ($_SESSION['errors'] as $error) { ?>
                                <li><?= $error[0] ?></li>
                            <?php } ?>
                        </ul>
                    </div>
                <?php } ?>
                <?php unset($_SESSION["errors"]);
            } ?>

            <?php if (isset($_SESSION['alert'])) { ?>
                <div class="alert alert-success">
                    <?= $_SESSION['alert'] ?>
                </div>
            <?php unset($_SESSION["alert"]);} ?>
            <input type="hidden" name="action" value="login_api">
            <div class="form-group">
                <label class="login-box__label d-block text-left" id="email-input">Email</label>
                <input type="text" name="email" class="form-control">
            </div>
            <div class="form-group mb-20">
                <label class="login-box__label d-block text-left" id="password-input">Password</label>
                <input type="password" name="password" class="form-control">
            </div>

            <div class="login-box__btn">
                <button type="submit" class="btn btn-signIn w240">SIGN IN</button>
            </div>
        </form>
        <div class="text-center">
                <a href="<?= get_site_url() . '/wp-admin/admin.php?page=password_reset_form'?>" class="forgot">Forgot Password?</a>
        </div>
<!--        <p class="login-box__txt-dont">Don’t have an account? <a href="#" class="register-link">Register Now</a></p>-->
    </div>
</div>