<header>
    <div class="row">
        <div class="col-12 p-3 text-right">
            <a href="<?=get_site_url() . '/wp-admin/admin.php?page=pp_settings'?>" class="fs17 c-black font-medium pl-2"><b>General Settings</b></a>
        </div>
    </div>
</header>
<div class="row">
    <div class="col-20">
        <div class="left-part h-100">
            <div>
                <ul class="left-list pb-4 mb-0">
                    <li><p class="fs14 c-grey font-medium mb-20">Templates</p></li>
                    <li><a href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect'?>" class="fs17 c-black font-medium pl-2"><b>Templates</b></a></li>
                </ul>

                <ul class="bottom-list">
                    <li class="mb-20"><p href="#" class="fs14 c-grey font-medium ">Templates</p></li>
                    <?php foreach($userPpPosts as $post){ ?>
                        <li class="mb-20 pl-2"><a class="fs17 d-inline-block font-medium c-black template-links" data-slug="<?=$post->post_name?>"
                                                  href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect' . '#' .$post->post_name?>"><?=$post->post_title?></a></li>
                    <?php }?>
                </ul>
                <p class="mb-20 pl-4"><a href="<?=get_site_url() . '/wp-admin/admin.php?page=prospect' . '#' . 'new_template'?>" class="fs17 d-inline-block font-medium temp-btn bgc-blue" >New Template</a></p>
            </div>
           <div class="pl-4">
               <a href="javascript:void(0)" class="fs17 d-inline-block font-medium report-btn bgc-red mb-20 position-relative"><i class="fas fa-file-alt doc-icon"></i> Reporting</a>
           </div>

        </div>
    </div>
    <div class="col-80">
        <div class="right-part h-100 content-div">
            <div class="separate-div">
                <?php if(isset($_SESSION['error'])){?>
                    <div class="alert alert-danger mt-2">
                        <ul>
                            <li><?= $_SESSION['error'] ?></li>
                        </ul>
                    </div>
                    <?php unset($_SESSION["error"]); } ?>

                <form method="get" action="<?php echo esc_url(admin_url('admin-post.php')); ?>">
                    <input type="hidden" name="action" value="filter_reports">

                    <div class="row align-items-end">
                        <div class="col-md-6">
                            <div class="d-flex align-items-end flex-wrap">
                                <div class="form-group">
                                    <label for="start_date" class="label">Start Date</label>
                                    <div class="position-relative">
                                        <input class="datepicker form-control" name="start_date" type="text" id="start_date" data-date-end-date="0d" autocomplete="off">
                                        <i class="far fa-calendar calendar-icon"></i>
                                    </div>

                                </div>
                                <div class="form-group">
                                    <label for="end_date" class="label">End Date</label>
                                    <div class="position-relative">
                                        <input class="datepicker form-control" name="end_date" type="text" id="end_date" autocomplete="off">
                                        <i class="far fa-calendar calendar-icon"></i>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-filter fs16 bgc-darkgray" id="filter_button" disabled>Filter</button>
                                </div>
                            </div>


                        </div>
                        <div class="col-md-6 text-right">
                            <button type="button" class="btn btn-csv bgc-lgreen fs16 position-relative" onclick="event.preventDefault();
                                                     document.getElementById('exportForm').submit();">
                                <i class="fas fa-file-alt doc-csv-icon"></i> Download CSV
                            </button>
                        </div>
                    </div>
                </form>
                <form id="exportForm" method="get" action="<?php echo esc_url(admin_url('admin-post.php')); ?>">
                    <input type="hidden" name="action" value="csv_export">
                    <input type="hidden" name="start_date" value="<?=isset($_GET['start_date']) ? $_GET['start_date'] : ''?>">
                    <input type="hidden" name="end_date" value="<?=isset($_GET['end_date']) ? $_GET['end_date'] : ''?>">
                </form>
            </div>

            <div class="separate-div">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover table-condensed">
                        <thead>
                        <tr>
                            <th>Time/Date</th>
                            <th>Page</th>
                            <th>Email</th>
                            <th>Name</th>
                            <th>Company</th>
                            <th>Total Visits</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php if(isset($reports) && $reports){?>
                            <?php foreach($reports as $report){?>
                                <tr>
                                    <td><?= $report->created_at ?></td>
                                    <td><a href="<?= (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https://" : "http://") . $_SERVER['HTTP_HOST'] . '/pp/' . $report->page_slug ?>" target="_blank" class="link"><?= $report->page_title ?></a></td>
                                    <td><?= $report->email ?></td>
                                    <td><?= $report->name ?></td>
                                    <td><?= $report->company ?></td>
                                    <td><?= $report->visit_count ?></td>
                                </tr>
                            <?php }?>
                        <?php }?>
                        </tbody>
                    </table>
                </div>

                <?php if(isset($reports) && $reports){?>
                    <?php echo $pagination; ?>
                <?php }?>
            </div>
        </div>

    </div>

</div>


