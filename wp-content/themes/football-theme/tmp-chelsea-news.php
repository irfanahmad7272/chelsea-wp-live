<?php
/*
 * Template Name: Chelsea News
 */

get_header();

if (isMobileDevice()) {
    include_once 'layouts/chelsea-news-page-mobile-view.php';
} else {

    include_once 'layouts/chelsea-news-page-desktop-view.php';
}
?>


<?php
get_footer();

