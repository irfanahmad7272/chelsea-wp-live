<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package United_Theme
 */
?>
<footer class="footer">
    <div class="container">
        <span class="text-muted text-center d-block">© ChelseaNytt International Ltd. 2020</span>
    </div>
</footer>

<?php wp_footer(); ?>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/jquery-3.4.1.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo get_template_directory_uri() ?>/assets/js/scripts.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function () {

        //load more for fetched articles.
        jQuery('.load-more-fetched-article-btn').on('click', function () {

            let limit = jQuery(this).data('limit');
            let offset = jQuery(this).data('offset');
            let newOffset = limit + offset;
            jQuery.ajax({
                type: "post",
                url: "<?php echo admin_url() ?>/admin-ajax.php",
                data: {
                    action: 'load_more_fetched_article_ajax',
                    limit: limit,
                    offset: offset
                },
                success: function (html) {
                    if (html) {
                        jQuery('div.load-more-fetched-article-section').append(html);

                        //update new offset.
                        jQuery('.load-more-fetched-article-btn').data('offset', newOffset);
                    }
                }
            });
        });


        //load more for our own articles/news
        jQuery('.load-more-own-article-btn').on('click', function () {

            let limit = jQuery(this).data('limit');
            let offset = jQuery(this).data('offset');
            let newOffset = limit + offset;
            jQuery.ajax({
                type: "post",
                url: "<?php echo admin_url() ?>/admin-ajax.php",
                data: {
                    action: 'load_more_own_article_ajax',
                    limit: limit,
                    offset: offset
                },
                success: function (html) {
                    if (html) {
                        jQuery('div.load-more-own-article-section').append(html);

                        //update new offset.
                        jQuery('.load-more-own-article-btn').data('offset', newOffset);
                    }
                }
            });
        });


        //sticky header feature.

        if (window.matchMedia("(max-width: 480px)").matches) {

            let lastScrollTop = 0;
            jQuery(window).scroll(function (event) {
                var st = jQuery(this).scrollTop();
                if (st > lastScrollTop && st > 100) {

                    jQuery('.header.sticky-mobile-header').addClass('sticky-mobile-header-translated');

                } else {

                    jQuery('.header.sticky-mobile-header').removeClass('sticky-mobile-header-translated');

                }
                lastScrollTop = st;
            });
        }
    });
</script>
</body>
</html>
