<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="generic-ads-box d-block d-sm-none">
    <div id='div-gpt-ad-panorama-2'>
        <script data-adfscript="adx.adform.net/adx/?mid=555306"></script>
        <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
    </div>
</div>
<div class="generic-ads-box d-none d-sm-block">
    <!--    <div id='div-gpt-ad-modul-0'></div>-->
</div>
<div class="related_articles_section">
    <?php if (get_post_meta(get_the_ID(), 'external_link_1_title', true)): ?>
        <h6>Ämnet i andra tidningar</h6>
    <?php endif; ?>
    <ul class="related_articles_list list-inline">
        <?php if (get_post_meta(get_the_ID(), 'external_link_1_title', true)): ?>
            <li>
                <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_1', true) ?? ""; ?>">
                    <?php echo get_post_meta(get_the_ID(), 'external_link_1_title', true) ?? ""; ?>
                    <span><?php echo get_post_meta(get_the_ID(), 'external_link_1_source', true) ?? ""; ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (get_post_meta(get_the_ID(), 'external_link_2_title', true)): ?>
            <li>
                <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_2', true) ?? ""; ?>">
                    <?php echo get_post_meta(get_the_ID(), 'external_link_2_title', true) ?? ""; ?>
                    <span><?php echo get_post_meta(get_the_ID(), 'external_link_2_source', true) ?? ""; ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (get_post_meta(get_the_ID(), 'external_link_3_title', true)): ?>
            <li>
                <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_3', true) ?? ""; ?>">
                    <?php echo get_post_meta(get_the_ID(), 'external_link_3_title', true) ?? ""; ?>
                    <span><?php echo get_post_meta(get_the_ID(), 'external_link_3_source', true) ?? ""; ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (get_post_meta(get_the_ID(), 'external_link_4_title', true)): ?>
            <li>
                <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_4', true) ?? ""; ?>">
                    <?php echo get_post_meta(get_the_ID(), 'external_link_4_title', true) ?? ""; ?>
                    <span><?php echo get_post_meta(get_the_ID(), 'external_link_4_source', true) ?? ""; ?></span>
                </a>
            </li>
        <?php endif; ?>
        <?php if (get_post_meta(get_the_ID(), 'external_link_5_title', true)): ?>
            <li>
                <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_5', true) ?? ""; ?>">
                    <?php echo get_post_meta(get_the_ID(), 'external_link_5_title', true) ?? ""; ?>
                    <span><?php echo get_post_meta(get_the_ID(), 'external_link_5_source', true) ?? ""; ?></span>
                </a>
            </li>
        <?php endif; ?>
    </ul>
</div>
<div class="d-none d-sm-block">
    <?php
//home page view for single article page.
    $top_posts = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 10,
        'offset' => 0,
        'post__not_in' => array(get_the_ID())
    ));

    if ($top_posts->have_posts()):
        echo '<p class="single-page-top-posts">Senaste nytt</p>';
        ?>
        <?php generateLargeArticle($top_posts->posts[0]); ?>
        <?php articleSeparator(); ?>
        <?php generateMediumArticle($top_posts->posts[1]); ?>
        <?php generateMediumArticle($top_posts->posts[2], 'right'); ?>
        <?php generateMediumArticle($top_posts->posts[3]); ?>
        <?php generateMediumArticle($top_posts->posts[4], 'right'); ?>
        <div class="generic-ads-box">
            <!--            <div id='div-gpt-ad-modul-1'></div>-->
        </div>

        <?php generateLargeArticle($top_posts->posts[5]); ?>
        <?php articleSeparator(); ?>
        <?php generateMediumArticle($top_posts->posts[6]); ?>
        <?php generateMediumArticle($top_posts->posts[7], 'right'); ?>
        <?php generateMediumArticle($top_posts->posts[8]); ?>
        <?php generateMediumArticle($top_posts->posts[9], 'right'); ?>
        <div class="generic-ads-box">
            <!--            <div id='div-gpt-ad-modul-2'></div>-->
        </div>
        <?php
    endif;
    ?>
</div>
<div class="d-block d-sm-none">
    <?php
//home page view for single article page.
    $top_posts = new WP_Query(array(
        'post_type' => 'post',
        'posts_per_page' => 11,
        'offset' => 0,
        'post__not_in' => array(get_the_ID())
    ));

    if ($top_posts->have_posts()):
        ?>
        <?php generateLargeArticle($top_posts->posts[0]); ?>
        <?php articleSeparator(); ?>
        <?php generateMediumArticle($top_posts->posts[1]); ?>
        <?php generateMediumArticle($top_posts->posts[2], 'right'); ?>
        <?php generateMediumArticle($top_posts->posts[3]); ?>
        <?php generateMediumArticle($top_posts->posts[4], 'right'); ?>
        <div class="generic-ads-box">
            <!--            <div id='div-gpt-ad-panorama-3'></div>-->
        </div>

        <?php generateLargeArticle($top_posts->posts[5]); ?>
        <?php articleSeparator(); ?>
        <?php generateMediumArticle($top_posts->posts[6]); ?>
        <?php generateMediumArticle($top_posts->posts[7], 'right'); ?>
        <?php generateMediumArticle($top_posts->posts[8]); ?>
        <?php generateMediumArticle($top_posts->posts[9], 'right'); ?>
        <?php articleSeparator(); ?>
        <?php generateLargeArticle($top_posts->posts[10]); ?>
        <div class="generic-ads-box">
            <!--            <div id='div-gpt-ad-panorama-4'></div>-->
        </div>
        <?php
    endif;
    ?>
</div>
<!--  Flyer nyther section -->
<?php
//flyer nyther section for single article page.
$current_post_ID = get_the_ID();
$posts_offset = 11;
$increment = 4;
$more_posts = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'offset' => $posts_offset,
    'post_status' => 'publish',
    'post__not_in' => array($current_post_ID)
        ));
?>
<div class="more-news-section" data-offset="<?php echo $posts_offset + 1 ?>" data-increment="<?php echo $increment ?>" data-current="<?php echo $current_post_ID ?>">
    <?php if ($more_posts->have_posts()): ?>
        <?php foreach ($more_posts->posts as $post): ?>
            <?php generateMoreArticle($post); ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>