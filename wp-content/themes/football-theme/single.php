<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package United_Theme
 */

get_header();

if (isMobileDevice()) {
    
    get_template_part('layouts/single-page-mobile-view-football');
    
} else {

    get_template_part('layouts/single-page-desktop-view-football');

}

get_footer();