<?php
/**
 * United Theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package United_Theme
 */
if (!function_exists('united_theme_setup')) :

    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function united_theme_setup() {
        /*
         * Make theme available for translation.
         * Translations can be filed in the /languages/ directory.
         * If you're building a theme based on United Theme, use a find and replace
         * to change 'united-theme' to the name of your theme in all the template files.
         */
        load_theme_textdomain('united-theme', get_template_directory() . '/languages');

        // Add default posts and comments RSS feed links to head.
        add_theme_support('automatic-feed-links');

        /*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
        add_theme_support('title-tag');

        /*
         * Enable support for Post Thumbnails on posts and pages.
         *
         * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
         */
        add_theme_support('post-thumbnails');

        // This theme uses wp_nav_menu() in one location.
        register_nav_menus(array(
            'primary' => esc_html__('Primary', 'united-theme'),
        ));

        /*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
        add_theme_support('html5', array(
            'search-form',
            'comment-form',
            'comment-list',
            'gallery',
            'caption',
        ));

        // Set up the WordPress core custom background feature.
        add_theme_support('custom-background', apply_filters('united_theme_custom_background_args', array(
            'default-color' => 'ffffff',
            'default-image' => '',
        )));

        // Add theme support for selective refresh for widgets.
        add_theme_support('customize-selective-refresh-widgets');

        /**
         * Add support for core custom logo.
         *
         * @link https://codex.wordpress.org/Theme_Logo
         */
        add_theme_support('custom-logo', array(
            'height' => 250,
            'width' => 250,
            'flex-width' => true,
            'flex-height' => true,
        ));
    }

endif;
add_action('after_setup_theme', 'united_theme_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function united_theme_content_width() {
    // This variable is intended to be overruled from themes.
    // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
    // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
    $GLOBALS['content_width'] = apply_filters('united_theme_content_width', 640);
}

add_action('after_setup_theme', 'united_theme_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function united_theme_widgets_init() {
    register_sidebar(array(
        'name' => esc_html__('Sidebar', 'united-theme'),
        'id' => 'sidebar-1',
        'description' => esc_html__('Add widgets here.', 'united-theme'),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget' => '</section>',
        'before_title' => '<h2 class="widget-title">',
        'after_title' => '</h2>',
    ));
}

add_action('widgets_init', 'united_theme_widgets_init');

add_filter('rest_authentication_errors', function( $result ) {
    if (!empty($result)) {
        return $result;
    }
    if (!is_user_logged_in()) {
        return new WP_Error('rest_not_logged_in', 'You are not currently logged in.', array('status' => 401));
    }
    return $result;
});

function wpb_disable_feed() {
    wp_die(__('No feed available,please visit our <a href="' . get_bloginfo('url') . '">homepage</a>!'));
}

add_action('do_feed', 'wpb_disable_feed', 1);
add_action('do_feed_rdf', 'wpb_disable_feed', 1);
add_action('do_feed_rss', 'wpb_disable_feed', 1);
add_action('do_feed_rss2', 'wpb_disable_feed', 1);
add_action('do_feed_atom', 'wpb_disable_feed', 1);
add_action('do_feed_rss2_comments', 'wpb_disable_feed', 1);
add_action('do_feed_atom_comments', 'wpb_disable_feed', 1);

/**
 * Enqueue scripts and styles.
 */
function united_theme_scripts() {
//	wp_enqueue_style( 'united-theme-style', get_stylesheet_uri() );

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
    wp_localize_script('jquery-scripts', 'united_script', array('ajax_url' => admin_url('admin-ajax.php')));
}

add_action('wp_enqueue_scripts', 'united_theme_scripts');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
    require get_template_directory() . '/inc/jetpack.php';
}

function getPostImage($post_ID, $size = '') {
    if (get_the_post_thumbnail_url($post_ID, $size)) {
        return get_the_post_thumbnail_url($post_ID, $size);
    } else {
        return get_template_directory_uri() . '/assets/images/img-placeholder.png';
    }
}

function get_post_formatted_date($datestream, $date, $ID) {
    if ($datestream >= strtotime("today")) {
        echo "Idag " . get_the_date('H.i', $ID);
    } else if ($datestream >= strtotime("yesterday")) {
        echo "Igår " . get_the_date('H.i', $ID);
    } else {
        echo $date;
    }
}

function generateLargeArticle($post_p) {
    ?>
    <div class="col-12 mb-0 mb-sm-3 own-article-large">
        <div class="post feature-post">
            <div class="post-thumbnail">
                <img src="<?php echo getPostImage($post_p->ID, 'full'); ?>" alt="">
            </div>
            <div class="post-info overlay">
                <h2>
                    <a href="<?php echo get_the_permalink($post_p->ID) ?>"> <?php echo get_the_title($post_p->ID) ?> </a>
                </h2>
                <ul class="post-tags d-sm-flex d-none list-inline">
                    <li><i class="fas fa-check color-light-blue"></i> <?php echo getArticleShortSummary($post_p->ID, 75); ?></li>
                </ul>
                <ul class="post-tags d-flex d-sm-none list-inline">
                    <li><i class="fas fa-check color-light-blue"></i> <?php echo getArticleShortSummary($post_p->ID, 30); ?></li>
                </ul>
            </div>
        </div>
    </div> <!-- col -->
    <?php
}

function generateSmallArticle($post) {
    ?>
    <div class="main-article small-post collapsed-article">
        <article class="full-article" style="">
            <div class="main-container">
                <div class="sm-image-section">
                    <div class="post-thumbnail">
                        <a href="<?php echo get_the_permalink($post->ID) ?>" class="img-link">
                            <img src="<?php echo getPostImage($post->ID, 'medium'); ?>"
                                 class="img-fluid small-img lg-expand-to-full" alt="" />
                        </a>
                    </div>
                    <div class="c-post-meta d-inline-block float-right small-post-comment">
                        <?php if (get_comments_number($post->ID)): ?>
                            <a href="<?php echo get_the_permalink($post->ID) ?>"
                               class="comments"><?php echo get_comments_number($post->ID) ?></a>
                           <?php endif; ?>
                    </div>
                    <span
                        class="date sm-date"><?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', $post->ID)), get_the_date('Y-m-d', $post->ID), $post->ID); ?></span>
                </div>
                <div class="sm-title-section">
                    <a href="<?php echo get_the_permalink($post->ID) ?>"
                       class="title lg-expand-to-full"><?php echo get_the_title($post->ID) ?></a>
                </div>
                <?php articleContentSection($post); ?>
            </div> <!-- full main box-->
        </article>
    </div> <!-- post small -->
    <?php
}

function generateMediumArticle($post) {
    ?>
    <div class="col-md-4 mb-3">

        <div class="default-post-wrap">
            <div class="category-name">Chelsea</div>
            <div class="post-thumbnail">
                <a href="<?php echo get_the_permalink($post->ID) ?>"><img
                        src="<?php echo getPostImage($post->ID, 'full'); ?>" alt=""></a>
            </div>
            <div class="post-info default">
                <div class="date">
                    <?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', $post->ID)), get_the_date('Y-m-d', $post->ID), $post->ID); ?>
                </div>
                <h4 class="post-title">
                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                        <?php echo get_the_title($post->ID) ?>
                    </a>
                </h4>
                <a href="<?php echo get_the_permalink($post->ID) ?>" style="text-decoration: none;">
                    <div class="post-desc">

                        <i class="fas fa-check color-light-blue"></i> <?php echo getArticleShortSummary($post->ID, 55); ?>

                    </div>
                </a>
            </div>
        </div>

    </div> <!-- col -->
    <?php
}

function generateMediumArticleMobile($post) {
    ?>
    <div class="col-12 own-article-mobile pl-0 pr-0">
        <div class="row">
            <div class="col-6 p-0">
                <div class="post-thumbnail">
                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                        <img src="<?php echo getPostImage($post->ID, 'full'); ?>" alt="">
                    </a>
                </div>
            </div>
            <div class="col-6 pl-3 pr-0">
                <div class="post-info">
                    <div class="date">
                        <?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', $post->ID)), get_the_date('Y-m-d', $post->ID), $post->ID); ?>
                    </div>
                    <h4 class="post-title">
                        <a href="<?php echo get_the_permalink($post->ID) ?>">
                            <?php echo getArticleShortDescription(get_the_title($post->ID), 50) ?>
                        </a>
                    </h4>

                    <div class="post-desc">
                        <a href="<?php echo get_the_permalink($post->ID) ?>" style="text-decoration: none;">

                            <i class="fas fa-check text-danger"></i> <?php echo getArticleShortSummary($post->ID, 50); ?>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </div>

    <?php
}

function generateMoreArticle($post) {
    ?>
    <div class="main-article more-post d-none d-sm-block">
        <article class="full-article" style="">
            <div class="main-container">
                <div class="mp-image-section">
                    <div class="post-thumbnail">
                        <a href="<?php echo get_the_permalink($post->ID) ?>" class="img-link">
                            <img src="<?php echo getPostImage($post->ID, 'medium'); ?>"
                                 class="img-fluid small-img lg-expand-to-full" alt="" />
                        </a>
                    </div>
                </div>
                <div class="mp-title-section">
                    <span
                        class="date sm-date"><?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', $post->ID)), get_the_date('Y-m-d', $post->ID), $post->ID); ?></span>
                    <a href="<?php echo get_the_permalink($post->ID) ?>"
                       class="title lg-expand-to-full"><?php echo get_the_title($post->ID) ?></a>
                       <?php articleContentSectionMore($post); ?>
                </div>
            </div> <!-- full main box-->
        </article>
    </div> <!-- post more -->
    <div class="main-article medium-post d-block d-sm-none">
        <article class="full-article" style="">
            <div class="main-container">
                <div class="md-image-section">
                    <div class="post-thumbnail">
                        <a href="<?php echo get_the_permalink($post->ID) ?>" class="img-link">
                            <img src="<?php echo getPostImage($post->ID, 'medium'); ?>"
                                 class="img-fluid small-img lg-expand-to-full" alt="" />
                        </a>
                    </div>
                    <div class="c-post-meta d-inline-block float-right medium-post-comment">
                        <?php if (get_comments_number($post->ID)): ?>
                            <a href="<?php echo get_the_permalink($post->ID) ?>"
                               class="comments"><?php echo get_comments_number($post->ID) ?></a>
                           <?php endif; ?>
                    </div>
                    <span
                        class="date sm-date"><?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', $post->ID)), get_the_date('Y-m-d', $post->ID), $post->ID); ?></span>
                </div>
                <div class="md-title-section">
                    <a href="<?php echo get_the_permalink($post->ID) ?>"
                       class="title lg-expand-to-full"><?php echo get_the_title($post->ID) ?></a>
                </div>
                <?php articleContentSectionMedium($post); ?>
            </div> <!-- full main box-->
        </article>
    </div>
    <?php
}

function load_more_fetched_article_ajax() {
    global $wpdb;
    if (isset($_POST['offset']) && isset($_POST['limit'])) {
        $new_offset = (int) $_POST['offset'];
        $limit = (int) $_POST['limit'];
        $more_posts = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit {$limit} OFFSET {$new_offset}");
        ?>
        <?php if ($more_posts): ?>
            <?php foreach ($more_posts as $post): ?>
                <?php
                if (isMobileDevice()) {
                    generate_custom_small_article_mobile($post);
                } else {

                    generate_custom_medium_article($post);
                }
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php
    }

    wp_die();
}

add_action('wp_ajax_load_more_fetched_article_ajax', 'load_more_fetched_article_ajax');
add_action('wp_ajax_nopriv_load_more_fetched_article_ajax', 'load_more_fetched_article_ajax');

function load_more_own_article_ajax() {
    global $wpdb;
    if (isset($_POST['offset']) && isset($_POST['limit'])) {

        $new_offset = (int) $_POST['offset'];
        $limit = (int) $_POST['limit'];

        $more_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => $limit,
            'offset' => $new_offset,
            'post_status' => 'publish'
        ));
        ?>
        <?php if ($more_posts->have_posts()): ?>
            <?php foreach ($more_posts->posts as $post): ?>
                <?php
                if (isMobileDevice()) {

                    generateMediumArticleMobile($post);
                } else {

                    generateMediumArticle($post);
                }
                ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php
    }

    wp_die();
}

add_action('wp_ajax_load_more_own_article_ajax', 'load_more_own_article_ajax');
add_action('wp_ajax_nopriv_load_more_own_article_ajax', 'load_more_own_article_ajax');

function load_more_news_ajax() {
    if (isset($_POST['offset']) && isset($_POST['increment'])) {
        $new_offset = (int) $_POST['offset'];
        $more_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => $_POST['increment'],
            'offset' => $new_offset,
            'post_status' => 'publish'
        ));
        ?>
        <?php if ($more_posts->have_posts()): ?>
            <?php articleSeparator(); ?>
            <?php foreach ($more_posts->posts as $post): ?>
                <?php generateMoreArticle($post); ?>
            <?php endforeach; ?>
        <?php endif; ?>
        <?php
    }

    wp_die();
}

add_action('wp_ajax_load_more_news_ajax', 'load_more_news_ajax');
add_action('wp_ajax_nopriv_load_more_news_ajax', 'load_more_news_ajax');

//set post view when clicked/read.
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function wpb_set_post_views_last_7days($post_id) {

    $post_view_count = get_post_meta($post_id, 'view_count', true);
    $post_daily_view_count = get_post_meta($post_id, 'view_daily_counts', true);
    $post_recent_count = get_post_meta($post_id, 'view_recent_count', true);

    if ($post_view_count == '') {
        update_post_meta($post_id, 'view_count', 1);
    } else {
        $post_view_count = intval($post_view_count);
        ++$post_view_count;
        update_post_meta($post_id, 'view_count', $post_view_count);
    }

    $today = new DateTime;
    $days_to_consider = 7; // How many days to consider when looking for recent views?
    $delete_befre_date = new DateTime("-{$days_to_consider} days");

// Create the meta entry if doesn't already exist
    if ($post_daily_view_count == '') {
        $post_daily_view_count = array($today->format('Y-m-d') => 1);

// Otherwise, update the existing one
    } else { // if ( $post_daily_view_count == '' )
        // Update the entry for today
        $post_daily_view_count[$today->format('Y-m-d')] = intval($post_daily_view_count[$today->format('Y-m-d')]);
        ++$post_daily_view_count[$today->format('Y-m-d')];
    } // if ( $post_daily_view_count == '' )
// Calculate the recent total, removing entries older than X days from the recent counts
    $recent_count = 0;
    foreach (array_keys($post_daily_view_count) as $date) {
        if ($date < $delete_befre_date->format('Y-m-d')) {
            unset($post_daily_view_count[$date]);
        } else {
            $recent_count = $recent_count + $post_daily_view_count[$date];
        }
    }

// And save the update meta
    update_post_meta($post_id, 'view_daily_counts', $post_daily_view_count);
    update_post_meta($post_id, 'view_recent_count', $recent_count);
}

//To keep the count accurate, lets get rid of prefetching
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function getArticleShortSummary($p_id, $words) {
    $get_p = get_post($p_id);
    $p_excerpt = $get_p->post_excerpt;
    $p_content = $get_p->post_content;
    if ($p_excerpt != "") {
        //return wp_trim_words(get_the_excerpt($ID), $words);
        return mb_substr(wp_strip_all_tags($p_excerpt), 0, $words) . ' ...';
    } else {

        //return wp_trim_words($post_content, $words);
        return mb_substr(wp_strip_all_tags($p_content), 0, $words) . ' ...';
    }
}

function relatedArticleSection($post) {
    ?>
    <div class="related_articles_section">
        <h6>Ämnet i andra tidninga</h6>
        <ul class="related_articles_list list-inline">
            <?php if (get_post_meta($post->ID, 'external_link_1_title', true)): ?>
                <li>
                    <a href="<?php echo get_post_meta($post->ID, 'external_link_1', true) ?? ""; ?>">
                        <?php echo get_post_meta($post->ID, 'external_link_1_title', true) ?? ""; ?>
                        <span><?php echo get_post_meta($post->ID, 'external_link_1_source', true) ?? ""; ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (get_post_meta($post->ID, 'external_link_2_title', true)): ?>
                <li>
                    <a href="<?php echo get_post_meta($post->ID, 'external_link_2', true) ?? ""; ?>">
                        <?php echo get_post_meta($post->ID, 'external_link_2_title', true) ?? ""; ?>
                        <span><?php echo get_post_meta($post->ID, 'external_link_2_source', true) ?? ""; ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (get_post_meta($post->ID, 'external_link_3_title', true)): ?>
                <li>
                    <a href="<?php echo get_post_meta($post->ID, 'external_link_3', true) ?? ""; ?>">
                        <?php echo get_post_meta($post->ID, 'external_link_3_title', true) ?? ""; ?>
                        <span><?php echo get_post_meta($post->ID, 'external_link_3_source', true) ?? ""; ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (get_post_meta($post->ID, 'external_link_4_title', true)): ?>
                <li>
                    <a href="<?php echo get_post_meta($post->ID, 'external_link_4', true) ?? ""; ?>">
                        <?php echo get_post_meta($post->ID, 'external_link_4_title', true) ?? ""; ?>
                        <span><?php echo get_post_meta($post->ID, 'external_link_4_source', true) ?? ""; ?></span>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (get_post_meta($post->ID, 'external_link_5_title', true)): ?>
                <li>
                    <a href="<?php echo get_post_meta($post->ID, 'external_link_5', true) ?? ""; ?>">
                        <?php echo get_post_meta($post->ID, 'external_link_5_title', true) ?? ""; ?>
                        <span><?php echo get_post_meta($post->ID, 'external_link_5_source', true) ?? ""; ?></span>
                    </a>
                </li>
            <?php endif; ?>
        </ul>
    </div>
    <?php
}

function articleInsideShare($post) {
    ?>
    <div class="article-share d-none d-sm-block">
        <h6>Dela artikeln med dina vänner</h6>
        <div class="d-flex">
            <a class="btn btn-block btn-facebook mr-2 share-button"
               href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink($post->ID) ?>&display=popup&ref=plugin&src=share_button"
               onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                <i class="fab fa-facebook-f"></i> Dela
            </a>
            <a class="btn btn-block btn-twitter ml-2 mt-0"
               href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink($post->ID)) ?>">
                <i class="fab fa-twitter"></i> Tweet
            </a>
        </div>
    </div>
    <div class="articl-share-mobile d-block d-sm-none">
        <p class="article-share-title">Dela med dina vänner</p>
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style" data-a2a-url="<?php echo get_the_permalink($post->ID) ?>"
             data-a2a-title="<?php echo get_the_title($post->ID) ?>">
            <a class="a2a_button_facebook"></a>
            <a class="a2a_button_facebook_messenger"></a>
            <a class="a2a_button_twitter"></a>
            <a class="a2a_button_email"></a>
            <a class="a2a_button_copy_link"></a>
        </div>
    </div>
    <div class="single-comment-section col-12 d-block d-sm-none" style="padding: 0">
        <div class="comment-left">
            <a class="comment-dropdown" href=""><?php echo get_comments_number($post->ID) ?> kommentarer Klicka för att
                kommentera</a>
        </div>
    </div>
    <?php
}

function articleBottomAd($post) {
    ?>
    <div class="article-bottom-part">
        <div class="col-12 article-bottom-ad" style="display: none">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-2.jpg" class="img-fluid" alt="">
        </div>
    </div>
    <?php
}

function articleSideSection($post) {
    ?>
    <div class="expand-article-sidebar" style="display: none">
        <a class="close-lg-expanded-article-btn close-exp-btn"><i class="far fa-times-circle"></i></a>
        <div class="side-section">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/add-3.jpg" class="img-100" alt="">
        </div> <!-- side section -->
        <div class="side-section">
            <div class="article-share">
                <h6>Dela artikeln med dina vänner</h6>
                <div class="d-flex">
                    <a class="btn btn-block btn-facebook mr-2 share-button"
                       href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink($post->ID) ?>&display=popup&ref=plugin&src=share_button"
                       onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                        <i class="fab fa-facebook-f"></i> Dela
                    </a>
                    <a class="btn btn-block btn-twitter ml-2 mt-0"
                       href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink($post->ID)) ?>">
                        <i class="fab fa-twitter"></i> Tweet
                    </a>
                </div>
            </div> <!-- article share -->
        </div> <!-- side section -->
    </div><!-- col 4 -->
    <?php
}

function articleContentSectionLarge($_id) {
    ?>
    <div class="text-content lg-expand-to-full">
        <div class="short-text-content">
            <div class="d-none d-sm-inline">
                <?php echo getArticleShortSummary($_id, 65); ?>
            </div>
            <div class="d-inline d-sm-none">
                <?php echo getArticleShortSummary($_id, 40); ?>
            </div>
        </div>
    </div>
    <?php
}

function articleContentSection($post) {
    ?>
    <div class="text-content lg-expand-to-full">
        <div class="short-text-content">
            <div class="d-none d-sm-inline">
                <?php echo getArticleShortSummary($post->ID, 50); ?>
            </div>
            <div class="d-inline d-sm-none">
                <?php echo getArticleShortSummary($post->ID, 7); ?>
            </div>
        </div>
    </div>
    <?php
}

function articleContentSectionMedium($post) {
    ?>
    <div class="text-content lg-expand-to-full">
        <div class="short-text-content">
            <div class="d-none d-sm-inline">
                <?php echo getArticleShortSummary($post->ID, 30); ?>
            </div>
            <div class="d-inline d-sm-none">
                <?php echo getArticleShortSummary($post->ID, 30); ?>
            </div>
        </div>
    </div>
    <?php
}

function articleContentSectionMore($post) {
    ?>
    <div class="text-content lg-expand-to-full">
        <div class="short-text-content">
            <div class="d-none d-sm-inline">
                <?php echo getArticleShortSummary($post->ID, 30); ?>
            </div>
            <div class="d-inline d-sm-none">
                <?php echo getArticleShortSummary($post->ID, 7); ?>
            </div>
        </div>
    </div>
    <?php
}

function articleSeparator() {
    ?>
    <div class="article-separator"></div>
    <?php
}

function generateAdvertisement($class = "", $src = "") {
    if (empty($src)):
        $src = get_template_directory_uri() . '/assets/images/ads-images/320x320.png';
    endif;
    ?>
    <div class="generic-ads-box <?php echo $class ?>">
        <img src="<?php echo $src ?>" />
    </div>
    <?php
}

function isMobileDevice() {
    $aMobileUA = array(
        '/iphone/i' => 'iPhone',
        '/ipod/i' => 'iPod',
        '/ipad/i' => 'iPad',
        '/android/i' => 'Android',
        '/blackberry/i' => 'BlackBerry',
        '/webos/i' => 'Mobile'
    );

    //Return true if Mobile User Agent is detected
    foreach ($aMobileUA as $sMobileKey => $sMobileOS) {
        if (preg_match($sMobileKey, $_SERVER['HTTP_USER_AGENT'])) {
            return true;
        }
    }
    //Otherwise return false..  
    return false;
}

function get_article_formatted_date($datestream, $date) {

    $date = date('d/m/Y G:i', $datestream);
    $d = explode(' ', $date);

    if ($datestream >= strtotime("today")) {
        echo "Idag " . $d[1];
    } else if ($datestream >= strtotime("yesterday")) {
        echo "Igår " . $d[1];
    } else {
        echo $date;
    }
}

function get_article_source_img_url($source) {
    $source_img = "";

    if ($source === "Sky Sports") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/sky-sports.png";
    }

    if ($source === "Mirror") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/mirror.png";
    }

    if ($source === "Daily Mail") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/dailymail.png";
    }

    if ($source === "BBC") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/bbc.png";
    }

    if ($source === "Evening Standard") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/eveningstandard.png";
    }

    if ($source === "Metro") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/metro.png";
    }

    if ($source === "Express") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/express.png";
    }

    if ($source === "Daily Star") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/dailystar.png";
    }

    if ($source === "The Sun") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/thesun.png";
    }

    if ($source === "Chelseafc") {
        $source_img = get_template_directory_uri() . "/assets/images/logoes/chelseafc.png";
    }


    return $source_img;
}

function get_fetched_article_img_url($url) {

    if ($url === "") {

        $url = get_template_directory_uri() . "/assets/images/chelsea-default-img.png";
    }

    return $url;
}

function generate_custom_medium_article($article) {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="col-md-4 mb-3">
        <div class="default-post-wrap">
            <div class="post-thumbnail">
                <img src="<?php echo get_fetched_article_img_url($article->img_url) ?>" alt="" data-league-name="<?php echo $article->league_name ?>">
            </div>
            <div class="post-info default">
                <div class="date d-flex mb-3">
                    <span> <?php echo $article->source_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> </span>
                    <span class="ml-auto"> <?php echo $article->team_name ?> </span>
                </div>
                <h4 class="post-title">
                    <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 75) ?>
                    </a>
                </h4>
                <div class="post-source">
                    <span>
                        <img src="<?php echo get_article_source_img_url($article->source_name) ?>" alt="" />
                    </span>
                </div>
            </div>
        </div>
    </div> <!-- col -->
    <?php
}

function generate_custom_medium_article_mobile($article) {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="col-12 mb-3 fetched-large-article-mobile">
        <div class="default-post-wrap">
            <div class="post-thumbnail position-relative">
                <img src="<?php echo get_fetched_article_img_url($article->img_url) ?>" alt="" data-league-name="<?php echo $article->league_name ?>">
            </div>

            <span class="post-source position-absolute">
                <img class="<?php ($article->source_name === "Chelseafc") ? 'chelsea-source-img' : '' ?>" src="<?php echo get_article_source_img_url($article->source_name) ?>" alt="" />
            </span>

            <div class="post-info default">
                <div class="date d-flex mb-3">
                    <span>Publicerad:  <?php echo $article->source_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> </span>

                </div>
                <h4 class="post-title">
                    <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 65) ?>
                    </a>
                </h4>

            </div>
        </div>
    </div> <!-- col -->
    <?php
}

function generate_custom_small_article_mobile($article) {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="d-flex fetched-small-article-mobile">
        <div class="row">
            <div class="col-6">
                <div class="post-thumbnail">
                    <img src="<?php echo get_fetched_article_img_url($article->img_url) ?>" alt="">
                </div>
            </div>
            <div class="col-6">
                <div class="post-info">
                    <h5 class="post-title">
                        <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 55) ?>
                        </a>
                    </h5>
                    <div class="date"> <?php echo $article->source_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function generate_post_vertical_article($article) {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="d-flex">
        <div class="row">
            <div class="col-md-7">
                <div class="post-thumbnail">
                    <img src="<?php echo $article->img_url ?>" alt="">
                </div>
            </div>
            <div class="col-md-5">
                <div class="post-info">
                    <h5 class="post-title">
                        <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 35) ?>
                        </a>
                    </h5>
                    <div class="date"> <?php echo $article->team_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}

function generate_post_tiny_article($article, $border_color = '') {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="col-md-2 mb-3">
        <div class="post-small">
            <div class="headline">
                <span> <?php echo $article->team_name ?> </span>
            </div>
            <div class="post-thumbnail">
                <img src="<?php echo $article->img_url ?>" alt="">
            </div>
            <div class="post-info">
                <h5 class="post-title">
                    <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 25) ?>
                    </a>
                </h5>
                <div class="date <?php echo $border_color ?>"> <?php echo $article->source_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> </div>
            </div>
        </div>
    </div> <!-- col -->
    <?php
}

function generate_single_post_small_article($article) {
    $url = site_url() . "/football-article?id=" . $article->id;
    ?>
    <div class="col-lg-6">
        <div class="post-small">
            <div class="post-thumbnail">
                <img src="<?php echo $article->img_url ?>" alt="">
            </div>
            <div class="post-info">
                <h5 class="post-title">
                    <a href="<?php echo $url ?>"> <?php echo getArticleShortDescription($article->title, 25) ?>
                    </a>
                </h5>
                <div class="date <?php echo $border_color ?>"> <?php echo $article->source_name ?> -
    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date) ?> </div>
            </div>
        </div> <!-- post -->
    </div> <!-- col -->



    <?php
}

function getArticleShortTitle($title, $words) {

    return mb_substr(wp_strip_all_tags($title), 0, $words) . ' ...';
}

function getArticleShortDescription($content, $words) {
    $content = trim(wp_strip_all_tags($content));

    if (!empty($content)) {

        $length = strlen($content);
        if ($length <= $words) {

            return $content;
        } else {

            return mb_substr($content, 0, $words) . ' ...';
        }
    }
}

function getLeaguesMenu() {

    $leagues = get_terms(array(
        'taxonomy' => 'league',
        'hide_empty' => false,
        'hierarchical' => false,
        'parent' => 0,
        'orderby' => 'date',
        'order' => 'ASC',
    ));

    foreach ($leagues as $league):
        $league_link = get_term_link($league->term_id, 'league');
        $teams = get_term_children($league->term_id, 'league');
        ?>
        <li class="category-main-li">
            <a href="<?php echo $league_link ?>"><?php echo $league->name ?></a>
        <?php if ($teams): ?>
                <div class="dropdown" aria-hidden="true">
                    <ul role="menu">
            <?php foreach ($teams as $team_id): ?>
                    <?php $team = get_term_by('id', $team_id, 'league'); ?>
                            <li>
                                <a href="<?php echo get_term_link($team->term_id, 'league'); ?>"
                                   role="menuitem"><?php echo $team->name; ?></a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
        <?php endif; ?>

        </li>
        <?php
    endforeach;
}

function getVIAdvertisement() {
    ?>
    <script type="text/javascript">

        (function (v, i) {

            var scp = v.createElement("script"),
                    config = {

                        ChannelID: '5c4edc82073ef46aa86caa55',

                        AdUnitType: '2',

                        PublisherID: '666483826552581',

                        PlacementID: 'pltlyxmFhUtya6gjpul',

                        DivID: '',

                        IAB_Category: 'IAB17',

                        Keywords: '',

                        Language: 'en-us',

                        BG_Color: '',

                        Text_Color: '',

                        Font: '',

                        FontSize: '',

                    };

            scp.src = 'https://s.vi-serve.com/tagLoader.js';

            scp.type = "text/javascript";

            scp.async = true;

            scp.onload = function () {

                i[btoa('video intelligence start')].init(config);

            };

            (v.getElementsByTagName('head')[0] || v.documentElement.appendChild(v.createElement('head'))).appendChild(scp);

        })(document, window);

    </script>
    <?php
}
