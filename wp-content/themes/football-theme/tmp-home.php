<?php
/*
 * Template Name: Home Page
 */

get_header();

if (isMobileDevice()) {
    include_once 'layouts/home-page-mobile-view-football.php';
} else {

    include_once 'layouts/home-page-desktop-view-football.php';
}
?>


<?php
get_footer();

