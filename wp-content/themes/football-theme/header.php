<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package United_Theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <link rel="canonical" href="/">
        <meta property="og:url" content="<?php echo get_the_permalink() ?>" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="<?php echo get_the_title() ?>" />
        <meta property="og:description" content="<?php echo wp_trim_words($post->post_content, 20) ?>" />
        <meta property="og:image" content="<?php echo getPostImage($post->ID, 'medium'); ?>" />
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css2?family=Open+Sans+Condensed:wght@700&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa:300,400,500,600,700&display=swap" rel="stylesheet">
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_template_directory_uri() ?>/assets/style/vendors/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_template_directory_uri() ?>/assets/style/vendors/all.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_template_directory_uri() ?>/assets/style/custom-style/style.css" />

        <link rel="stylesheet" type="text/css"
              href="<?php echo get_template_directory_uri() ?>/assets/style/custom-style/style-single.css" />
        <link rel="stylesheet" type="text/css"
              href="<?php echo get_template_directory_uri() ?>/assets/style/custom-style/changes.css" />
              <?php wp_head(); ?>

    </head>

    <body <?php body_class(); ?>>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/sv_SE/sdk.js#xfbml=1&version=v3.2">
        </script>
        <header class="header sticky-mobile-header">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class=" align-items-center">
                            <div class="logo">
                                <a href="<?php echo site_url() ?>">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" />
                                </a>
                            </div>
<!--                            <div class="logo-right-mobile d-sm-none">
                                <img class="" src="<?php echo get_template_directory_uri() ?>/assets/images/logo-right.png" />
                            </div>-->
                            <div class="desktop-right-section d-block">
                                <ul class="top-right-menu">
                                    <li>
                                        <p class="fb-follow-desk-text d-none d-sm-block">Missa inget - följ oss</p>
                                        <p class="fb-follow-desk-text d-block d-sm-none">Följ oss och få Chelsea-nyheter i ditt flöde</p>
                                    </li>
                                    <li>
                                        <a href="#">

                                            <img class="fb" src="<?php echo get_template_directory_uri() ?>/assets/images/fb-icon.png" />
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#">
                                            <img class="insta" src="<?php echo get_template_directory_uri() ?>/assets/images/insta-icon.png" />
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </header> <!-- Header -->