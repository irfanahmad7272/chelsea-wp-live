<?php

/**
 * Template Name: single fetched artile
 */


get_header();


if (isMobileDevice()) {
    
    get_template_part('layouts/fetched-single-page-mobile-view-football');
    
} else {

    get_template_part('layouts/fetched-single-page-desktop-view-football');

}

get_footer();