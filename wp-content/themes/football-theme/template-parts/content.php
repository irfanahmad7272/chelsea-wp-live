<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package United_Theme
 */
?>

<article class="single-article">
    <div class="main-container">
        <div class="post-thumbnail">
            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" class="img-fluid large-img"
                alt="" />
        </div>

        <div class="article-share">
            <h6 class="d-none">Dela artikeln med dina vänner</h6>
            <div class="d-flex">
                <a class="btn btn-block btn-facebook mr-2 share-button"
                    href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                    onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                    <i class="fab fa-facebook-f"></i> Dela
                </a>
                <a class="btn btn-block btn-twitter ml-2 mt-0"
                    href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                    <i class="fab fa-twitter"></i> Tweet
                </a>

            </div>
        </div>

        <div class="full-detail">
            <a href="#" class="title"><?php echo get_the_title() ?></a>
            <div class="c-post-meta mb-3 d-flex">
                <div class="">

                    <span class="date">Skrivet av <?php the_author(); ?>
                        <?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', get_the_ID())), get_the_date('Y-m-d', get_the_ID()), get_the_ID()); ?></span>
                </div>
            </div>

            <div class="text-content">
                <div class="single-article-excerpt">
                    <?php echo get_the_excerpt(); ?>
                </div>
                <div class="generic-ads-box d-block d-sm-none">
                    <div class="video-ad-single-article-mobile"></div>

                </div>
                <div class="single-article-excerpt-2">
                    <?php if (get_post_meta(get_the_ID(), 'second_excerpt', true)): ?>

                    <?php echo get_post_meta(get_the_ID(), 'second_excerpt', true) ?? ""; ?>

                    <?php endif; ?>
                </div>
                <div class="generic-ads-box d-block d-sm-none">
                    <div id='div-gpt-ad-panorama-1'>
                        <script data-adfscript="adx.adform.net/adx/?mid=555305"></script>
                        <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                    </div>
                </div>
                <?php echo do_shortcode(wpautop(get_the_content(), true)); ?>
            </div>
        </div> <!-- short desc -->
    </div> <!-- full main box-->
    <div class="row d-flex d-sm-none" style="padding: 5px 0;margin: 0">
        <!--        <div class="video-ad-single-article-mobile">
        
                </div>-->
        <div class="single-share-section col-12" style="padding: 0">
            <p class="article-share-title d-none">Dela med dina vänner</p>
            <!-- AddToAny BEGIN -->
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_facebook_messenger"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_email"></a>
                <a class="a2a_button_copy_link"></a>
            </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
        </div>
        <div class="single-comment-section col-12" style="padding: 0">
            <div class="comment-left">
                <a class="comment-dropdown" href=""><?php echo get_comments_number(get_the_ID()) ?> kommentarer Klicka
                    för att kommentera</a>
            </div>
        </div>

    </div>
    <div class="row d-none d-sm-flex" style="margin: 0">
        <div class="single-share-section col-6" style="padding: 0">
            <!--            <p class="article-share-title">Gillade du artikeln? Dela<br>den med dina vänner</p>-->
            <!-- AddToAny BEGIN -->
            <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
                <a class="a2a_dd" href="https://www.addtoany.com/share"></a>
                <a class="a2a_button_facebook"></a>
                <a class="a2a_button_facebook_messenger"></a>
                <a class="a2a_button_twitter"></a>
                <a class="a2a_button_email"></a>
                <a class="a2a_button_copy_link"></a>
            </div>
            <script async src="https://static.addtoany.com/menu/page.js"></script>
            <!-- AddToAny END -->
        </div>
        <div class="single-comment-section col-6" style="padding: 0">
            <div class="comment-left">
                <a class="comment-dropdown" href=""><?php echo get_comments_number(get_the_ID()) ?> kommentarer - klicka
                    här</a>
            </div>
        </div>

    </div>
</article>

<div class="single-article-separator-line"></div>