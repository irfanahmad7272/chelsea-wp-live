<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="main-area grey_bg">

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->

        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 17,
            'offset' => 0
        ));

        ?>
        <div class="row">

            <?php generateLargeArticle($top_posts->posts[0]); ?>

        </div> <!-- row -->

        <div class="row mb-1">

            <?php generateMediumArticle($top_posts->posts[1]); ?>
            <?php generateMediumArticle($top_posts->posts[2]); ?>
            <?php generateMediumArticle($top_posts->posts[3]); ?>
            <?php generateMediumArticle($top_posts->posts[4]); ?>


            <div class="col-md-8 mb-3">
                <div>
<!--                    <img src="<?php echo site_url() ?>/wp-content/uploads/2020/01/advertisement2.jpg" class="img-fluid"
                        alt="">-->
                    <?php getVIAdvertisement(); ?>
                </div><!-- advertisement -->
            </div> <!-- col -->
        </div> <!-- row -->
        
        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->

        

        <div class="row">
            <?php generateMediumArticle($top_posts->posts[5]); ?>
            <?php generateMediumArticle($top_posts->posts[6]); ?>
            <?php generateMediumArticle($top_posts->posts[7]); ?>
            <?php generateMediumArticle($top_posts->posts[8]); ?>
            <?php generateMediumArticle($top_posts->posts[9]); ?>
            <?php generateMediumArticle($top_posts->posts[10]); ?>
        </div> <!-- row -->

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->

        <div class="row">
            <?php generateMediumArticle($top_posts->posts[11]); ?>
            <?php generateMediumArticle($top_posts->posts[12]); ?>
            <?php generateMediumArticle($top_posts->posts[13]); ?>
            <?php generateMediumArticle($top_posts->posts[14]); ?>
            <?php generateMediumArticle($top_posts->posts[15]); ?>
            <?php generateMediumArticle($top_posts->posts[16]); ?>
        </div> <!-- row -->


        <div class="row load-more-own-article-section"></div>
        
        <div class="load-more-posts mb-3 load-more-own-article-btn" data-offset="12" data-limit="12">
            Ladda fler artiklar
        </div><!-- load more posts-->
        
        
        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->
       



    </div> <!-- main-area -->
</div> <!-- container -->