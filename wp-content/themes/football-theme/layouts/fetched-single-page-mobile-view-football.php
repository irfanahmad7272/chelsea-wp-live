<?php
global $wpdb;
$article_id = isset($_GET['id']) ? $_GET['id'] : "";
$article = $wpdb->get_row("select * from articles where id = {$article_id}");
?>

<div class="container-mobile container-mobile-single">
    <div class="main-area">


        <div class="generic-ads-box">
<!--            <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
        </div><!-- advertisement -->



        <div class="row">
            <?php
            while (have_posts()): the_post();
                ?>
                <div class="col-12 mb-3 single-page-main-content">
                    <div class="post feature-post">
                        <div class="post-thumbnail">
                            <img src="<?php echo $article->img_url ?>" alt="post image">
                        </div>
                        <div class="bg-white p-4 single-article-content">
                            <div class="social-btns d-flex">
                                <a class="btn-fb"
                                   href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                    <span class="fab fa-facebook-f"></span> Dela
                                </a>
                                <a class="btn-twitter"
                                   href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                                    <span class="fab fa-twitter"></span> Tweet
                                </a>
                            </div>
                            <div class="mb-4">
                                <span class="single-page-meta">Publicerad: <?php echo $article->source_name ?> -  
                                    <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date); ?>
                                </span>
                            </div>
                            <div class="mb-4">
                                <h1 class="font-weight-bold main-title"><?php echo $article->title ?></h1>
                            </div>
                            
                            <div class="mb-4">

<!--                                <img class="w-100" src="<?php echo get_template_directory_uri() ?>/assets/images/vi-ad.png" />-->
                                <?php getVIAdvertisement(); ?>
                                <!-- advertisement -->
                            </div>
                            
                            <div class="mt-2 mb-5">
                                <p><?php echo $article->description ?></p>
                            </div>

                        </div>
                        <div class="col-12 followus-section-fetched">
                            <div class="d-flex">
                                <div class="social-section">
                                    <div class="text">
                                        <a class="source-link" target="_blank" href="<?php echo $article->url ?>">Läs hela nyheten  </a>
                                        <i class=" text-danger fas fa-caret-right"></i>
                                    </div>

                                </div>
                                <div class="logo-source">
                                    <a target="_blank" href="<?php echo $article->url ?>">
                                        <img src="<?php echo get_article_source_img_url($article->source_name) ?>"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
                <?php
            endwhile;
            ?>
        </div> <!-- row -->


        <div class="row mb-4">
<!--            <div class="generic-ads-box">
                <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />
            </div> advertisement -->
        </div>




        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 4,
            'offset' => 0
        ));
        ?>
        <div class="row mb-1">
            <?php generateMediumArticleMobile($top_posts->posts[0]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[1]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[2]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[3]); ?>


        </div> <!-- row -->

        <div class="row">
            <div class="col-12">
                <p class="flyer-nyther-text">
                    <a href="<?php echo site_url() ?>/chelsea-news">
                        Fler nyheter från Chelsea-Nytt.se

                        <i class="fas fa-caret-right flyer-nyther-caret ml-3"></i>
                    </a>
                </p>
            </div>

            <div class="generic-ads-box">
<!--                <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
            </div><!-- advertisement -->

        </div>

        <div class="row fetched-articles-intro-section">
            <div class="col-7">
                <h4>Superbevakning</h4>
                <p>Här är allt om Chelsea ifrån alla relevanta källor på internet - hämtat i realtid. </p>
            </div>
            <div class="col-5">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/newspaper.png" />
            </div>
        </div>

        <?php
        $wpdb;

        $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 3");
        if ($skysports_articles):
            ?>


            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[0]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[1]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[2]); ?>
            </div> <!-- row -->

            <div class="row load-more-fetched-article-section"></div>

            <div class="row">
                <div class="col-12 load-more-posts mt-3 mb-3 load-more-fetched-article-btn" data-offset="3" data-limit="12">
                    Ladda fler artiklar
                </div><!-- load more posts-->

                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->
            </div>


        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->