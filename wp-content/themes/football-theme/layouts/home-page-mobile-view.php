<div class="container">
    <div class="main-site-container">
        <div class="centeral-container">
            <div class="row site-top-row site-top-row-sticky">
                <div class="col-12 d-block d-sm-none">
                    <?php get_template_part('tmp-article-slider-mobile'); ?>

                </div>
                <div class="col-12">
                    <div class="generic-ads-box d-block d-sm-none">
                        <!-- /14604472/unitedsverige_se/panorama_atf (320x320) -->
                        <div id='div-gpt-ad-panorama-0'>
                            <script data-adfscript="adx.adform.net/adx/?mid=555304"></script>
                            <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                        </div>
                    </div>
                </div> <!-- col -->
            </div> <!-- row -->
            <div class="row">

                <div class="col-lg-8 col-12 main-side d-block d-sm-none">
                    <div class="followUs d-block d-sm-none">
                        <div class="d-flex align-items-center">
                            <h6 class="d-none d-sm-block">Missa inget. Följ Unitedsverige</h6>
                            <div class="ml-auto social-follow-container">
                                <div class="social-follow">
                                    <a href="https://web.facebook.com/Unitedsverigese-129197640491950/" class="fb-icon" target="new"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="tw-icon" target="new"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="in-icon" target="new"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $top_posts = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 12,
                        'offset' => 0
                    ));
                    ?>
                    <?php if ($top_posts->have_posts()): ?>

                        <?php generateLargeArticle($top_posts->posts[0]); ?>
                        <?php generateLargeArticle($top_posts->posts[1]); ?>
                        <?php generateLargeArticle($top_posts->posts[2]); ?>

                        <div class="generic-ads-box">
                            <div class="video-ad-single-article-mobile"></div>
                        </div>
                        <div class="generic-ads-box">
                            <!-- /14604472/unitedsverige_se/panorama_0 (320x320) -->
                            <div id='div-gpt-ad-panorama-1'>
                                <script data-adfscript="adx.adform.net/adx/?mid=555305"></script>
                                <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                            </div>
                        </div>

                        <?php generateLargeArticle($top_posts->posts[3]); ?>
                        <?php generateLargeArticle($top_posts->posts[4]); ?>
                        <?php generateLargeArticle($top_posts->posts[5]); ?>

                        <div class="generic-ads-box">
                            <!-- /14604472/unitedsverige_se/panorama_0 (320x320) -->
                            <div id='div-gpt-ad-panorama-2'>
                                <script data-adfscript="adx.adform.net/adx/?mid=555306"></script>
                                <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                            </div>
                        </div>
                        <?php generateLargeArticle($top_posts->posts[6]); ?>
                        <?php generateLargeArticle($top_posts->posts[7]); ?>
                        <?php generateLargeArticle($top_posts->posts[8]); ?>
                        <div class="generic-ads-box">
                        </div>

                        <?php generateLargeArticle($top_posts->posts[9]); ?>
                        <?php generateLargeArticle($top_posts->posts[10]); ?>
                        <?php generateLargeArticle($top_posts->posts[11]); ?>
                        <div class="generic-ads-box">
                        </div>

                    <?php endif; ?>

                    <!--Flyer Nyther(More News) section on home page-->
                    <?php
                    $posts_offset = 12;
                    $increment = 4;
                    $more_posts = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 4,
                        'offset' => $posts_offset
                    ));
                    ?>
                    <div class="more-news-section" data-offset="<?php echo $posts_offset ?>" data-increment="<?php echo $increment ?>">
                        <h3 class="more-news-title">Fler Nyheter</h3>
                        <?php if ($more_posts->have_posts()): ?>
                            <?php foreach ($more_posts->posts as $post): ?>
                                <?php generateMoreArticle($post); ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div> <!-- col -->

            </div> <!-- row -->
        </div>
    </div>

</div> <!-- container -->