<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container-mobile">
    <div class="main-area">


        <div class="generic-ads-box">
<!--            <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
        </div><!-- advertisement -->

        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 9,
            'offset' => 0
        ));

        if ($top_posts->posts):
            ?>

            <div class="row">

                <?php generateLargeArticle($top_posts->posts[0]); ?>

            </div> <!-- row -->

            <div class="generic-ads-box">
<!--                <img class="" src="<?php echo get_template_directory_uri() ?>/assets/images/vi-ad.png" />-->
                <?php getVIAdvertisement(); ?>
            </div><!-- advertisement -->

            <div class="row mb-1">

                <?php generateMediumArticleMobile($top_posts->posts[1]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[2]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[3]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[4]); ?>

            </div> <!-- row -->

            <div class="row">
                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->

            </div>

            <div class="row mb-1">

                <?php generateMediumArticleMobile($top_posts->posts[5]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[6]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[7]); ?>
                <?php generateMediumArticleMobile($top_posts->posts[8]); ?>

            </div> <!-- row -->
            
            
            <div class="row load-more-own-article-section"></div>


            <div class="row">
                <div class="col-12 load-more-posts mt-3 mb-3 load-more-own-article-btn" data-offset="9" data-limit="12">
                    Ladda fler artiklar
                </div><!-- load more posts-->

                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->
            </div>

        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->