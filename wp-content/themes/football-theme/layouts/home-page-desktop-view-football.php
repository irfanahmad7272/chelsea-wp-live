<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="container">
    <div class="main-area grey_bg">

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->

        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'offset' => 0
        ));

        ?>
        <div class="row">

            <?php generateLargeArticle($top_posts->posts[0]); ?>

        </div> <!-- row -->

        <div class="row mb-1">

            <?php generateMediumArticle($top_posts->posts[1]); ?>
            <?php generateMediumArticle($top_posts->posts[2]); ?>
            <?php generateMediumArticle($top_posts->posts[3]); ?>
            <?php generateMediumArticle($top_posts->posts[4]); ?>


            <div class="col-md-8 mb-3">
                <div>
<!--                    <img src="<?php echo site_url() ?>/wp-content/uploads/2020/01/advertisement2.jpg" class="img-fluid"
                        alt="">-->
                    <?php getVIAdvertisement(); ?>
                </div><!-- advertisement -->
            </div> <!-- col -->
        </div> <!-- row -->
        
        <div class="row mb-1">
            <div class="col-12">
                <p class="flyer-nyther-read-more text-center">
                    <a href="<?php echo site_url() ?>/chelsea-news">
                        Fler nyheter från Chelsea-Nytt.se

                        <i class="fas fa-caret-right text-danger ml-3"></i>
                    </a>
                </p>
            </div>
        </div>
        
        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->
        
        <div class="mb-3 row">
            <div class="col-3">
                <h2 class="fetched-intro-title">Superbevakning</h2>
            </div>
            <div class="col-7">
                <p class="fetched-intro-desc">Här är allt om Chelsea ifrån alla relevanta källor på internet - hämtat i realtid. </p>
            </div>
            <div class="col-2">
                <img class="fetched-intro-logo" src="<?php echo get_template_directory_uri()  ?>/assets/images/logo-right.png" />
            </div>
        </div>

        <?php
        
        $wpdb;
        
         $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 12 OFFSET 0");
         if($skysports_articles):
        
        ?>

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[0]); ?>
            <?php generate_custom_medium_article($skysports_articles[1]); ?>
            <?php generate_custom_medium_article($skysports_articles[2]); ?>
            <?php generate_custom_medium_article($skysports_articles[3]); ?>
            <?php generate_custom_medium_article($skysports_articles[4]); ?>
            <?php generate_custom_medium_article($skysports_articles[5]); ?>
        </div> <!-- row -->

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[6]); ?>
            <?php generate_custom_medium_article($skysports_articles[7]); ?>
            <?php generate_custom_medium_article($skysports_articles[8]); ?>
            <?php generate_custom_medium_article($skysports_articles[9]); ?>
            <?php generate_custom_medium_article($skysports_articles[10]); ?>
            <?php generate_custom_medium_article($skysports_articles[11]); ?>
        </div> <!-- row -->

        <div class="row load-more-fetched-article-section"></div>
        <div class="load-more-posts mb-3 load-more-fetched-article-btn" data-offset="12" data-limit="12">
            Ladda fler artiklar
        </div><!-- load more posts-->
        
        
        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->
       

        <?php endif; ?>


    </div> <!-- main-area -->
</div> <!-- container -->