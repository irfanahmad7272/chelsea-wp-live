<div class="container">
    <div class="main-site-container">
        <div class="centeral-container">
            <div class="row site-top-row site-top-row-sticky">
                <div class="col-12">
                    <div id='responsive-panorama-top' class="d-none d-sm-block">
                        <!-- GPT AdSlot 1 for Ad unit 'Unitedsverige.se_Pan_LSMB_PanoTop' ### Size: [[980,240],[980,120],[980,300],[728,90]] -->
                        <div id='div-gpt-ad-5098123-1'>
                            <script>
                                googletag.cmd.push(function () {
                                    googletag.display('div-gpt-ad-5098123-1');
                                });
                            </script>
                        </div>
                        <!-- End AdSlot 1 -->
                    </div>
                </div> <!-- col -->
            </div> <!-- row -->
            <?php
            $top_posts = new WP_Query(array(
                'post_type' => 'post',
                'posts_per_page' => 16,
                'offset' => 0
            ));

            //var_dump($top_posts->post_count);
            ?>
            <div class="row">
                <div class="col-lg-8 col-12 main-side d-none d-sm-block">

                    <?php generateLargeArticle($top_posts->posts[0]); ?>
                    <?php generateLargeArticle($top_posts->posts[1]); ?>
                    <?php generateLargeArticle($top_posts->posts[2]); ?>


                </div>
                <div class="col-lg-4 d-none d-lg-block sidebar-section">
                    <div class="side-section">
                        <div class="followUs">
                            <div class="d-flex align-items-center">
                                <h6 class="d-none d-sm-block">Missa inget. Följ Unitedsverige</h6>
                                <div class="ml-auto social-follow-container">
                                    <div class="social-follow">
                                        <a href="https://web.facebook.com/Unitedsverigese-129197640491950/" class="fb-icon" target="new"><i class="fab fa-facebook"></i></a>
                                        <a href="#" class="tw-icon" target="new"><i class="fab fa-twitter"></i></a>
                                        <a href="#" class="in-icon" target="new"><i class="fab fa-instagram"></i></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="side-section">
                        <div class="generic-ads-box">
                            <!-- GPT AdSlot 2 for Ad unit 'Unitedsverige.se_TWS_S_Sidebar1' ### Size: [[300,250]] -->
                            <div id='div-gpt-ad-5098123-2'>
                                <script>
                                    googletag.cmd.push(function () {
                                        googletag.display('div-gpt-ad-5098123-2');
                                    });
                                </script>
                            </div>
                            <!-- End AdSlot 2 -->
                        </div>
                    </div>
                    <div class="side-section">
                        <div class="video-ad-single-article-desktop">
                        </div>
                    </div>
                    <div class="side-section">
                        <div class="section-title">
                            <h6>Mest lästa senaste veckan</h6>
                        </div>
                        <?php
                        $most_read_posts = new WP_Query(array(
                            'post_type' => 'post',
                            'meta_key' => 'view_recent_count',
                            'orderby' => 'meta_value_num',
                            'order' => 'DESC',
                            'posts_per_page' => 7
                        ));
                        ?>
                        <ul class="favourte_post_list">
                            <?php foreach ($most_read_posts->posts as $post): ?>
                                <li>
                                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                                        <span class="thumbnail">
                                            <img src="<?php echo getPostImage($post->ID, 'thumbnail'); ?>" alt="<?php echo get_the_title($post->ID) ?>" />
                                        </span>
                                        <span class="s-title"><?php echo get_the_title($post->ID) ?></span>
                                    </a>
                                </li>
                            <?php endforeach; ?>
                        </ul>
                    </div> <!-- sidebar section -->
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="generic-ads-box">
                        <div id='div-gpt-ad-7002914-1'>
                            <script>
                                googletag.cmd.push(function () {
                                    googletag.display('div-gpt-ad-7002914-1');
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-12 main-side d-none d-sm-block">
                    <?php generateLargeArticle($top_posts->posts[3]); ?>
                    <?php articleSeparator(); ?>
                    <?php generateMediumArticle($top_posts->posts[4]); ?>
                    <?php generateMediumArticle($top_posts->posts[5], 'right'); ?>
                    <?php generateMediumArticle($top_posts->posts[6]); ?>
                    <?php generateMediumArticle($top_posts->posts[7], 'right'); ?>

                </div>
                <div class="col-lg-4 d-none d-lg-block sidebar-section">

                    <div class="side-section">
                        <div class="generic-ads-box">
                            <!-- GPT AdSlot 4 for Ad unit 'Unitedsverige.se_TWS_LSBZ_Sidebar2' ### Size: [[300,250],[300,600],[160,600],[250,360]] -->
                            <div id='div-gpt-ad-5098123-4'>
                                <script>
                                    googletag.cmd.push(function () {
                                        googletag.display('div-gpt-ad-5098123-4');
                                    });
                                </script>
                            </div>
                            <!-- End AdSlot 4 -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="generic-ads-box">
                        <div id='div-gpt-ad-7002914-2'>
                            <script>
                                googletag.cmd.push(function () {
                                    googletag.display('div-gpt-ad-7002914-2');
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-12 main-side d-none d-sm-block">
                    <?php generateLargeArticle($top_posts->posts[8]); ?>
                    <?php articleSeparator(); ?>
                    <?php generateMediumArticle($top_posts->posts[9]); ?>
                    <?php generateMediumArticle($top_posts->posts[10], 'right'); ?>
                    <?php generateMediumArticle($top_posts->posts[11]); ?>
                    <?php generateMediumArticle($top_posts->posts[12], 'right'); ?>

                </div>
                <div class="col-lg-4 d-none d-lg-block sidebar-section">

                    <div class="side-section">
                        <div class="generic-ads-box">

                            <!-- End AdSlot 4 -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="generic-ads-box">
                        <div id='div-gpt-ad-7002914-3'>
                            <script>
                                googletag.cmd.push(function () {
                                    googletag.display('div-gpt-ad-7002914-3');
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-12 main-side d-none d-sm-block">
                    <?php generateLargeArticle($top_posts->posts[13]); ?>
                    <?php generateLargeArticle($top_posts->posts[14]); ?>
                    <?php generateLargeArticle($top_posts->posts[15]); ?>


                    <!--Flyer Nyther(More News) section on home page-->
                    <?php //get_template_part('tmp-more-news'); ?>

                </div> <!-- col -->
                <div class="col-lg-4 d-none d-lg-block sidebar-section">

                    <div class="side-section right-side-last-ad">
                        <div class="generic-ads-box">

                            <!-- End AdSlot 4 -->
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-12 col-md-12">
                    <div class="generic-ads-box">
                        <div id='div-gpt-ad-7002914-4'>
                            <script>
                                googletag.cmd.push(function () {
                                    googletag.display('div-gpt-ad-7002914-4');
                                });
                            </script>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-8 col-12 main-side d-none d-sm-block">
                    <!--Flyer Nyther(More News) section on home page-->
                    <?php get_template_part('tmp-more-news'); ?>

                </div> <!-- col -->
                <div class="col-lg-4 d-none d-lg-block sidebar-section">
                </div>
            </div> <!-- row -->
        </div>


        <div class="far-right-container">
            <div class="far-right-ads-box">
                <!-- GPT AdSlot 3 for Ad unit 'Unitedsverige.se_TWS_LSBZ_SidebarRight' ### Size: [[300,250],[300,600],[160,600],[250,360]] -->
                <div id='div-gpt-ad-5098123-3'>
                    <script>
                        googletag.cmd.push(function () {
                            googletag.display('div-gpt-ad-5098123-3');
                        });
                    </script>
                </div>
                <!-- End AdSlot 3 -->
            </div>
        </div>
    </div>

</div> <!-- container -->