<div class="container single-post-page">
    <div class="main-site-container">
        <div class="centeral-container">
            <div class="row site-top-row site-top-row-sticky">
                <div class="col-12">
                    <div id='responsive-panorama-top' class="d-none d-sm-block">
                        <div id='div-gpt-ad-panorama-atf'></div>
                    </div>
                </div> <!-- col -->
                <div class="generic-ads-box d-block d-sm-none single-page-top-sticky-ad-mobile sticky-top-ad">
                    <div id="single-page-top-sticky-ad-wrapper">
                        <div id='div-gpt-ad-panorama-0'>
                            <script data-adfscript="adx.adform.net/adx/?mid=555304"></script>
                            <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-8 col-12 main-side">
                    <div class="followUs d-none">
                        <div class="d-flex align-items-center">
                            <h6 class="d-none d-sm-block">Missa inget. Följ Unitedsverige</h6>
                            <div class="ml-auto social-follow-container">
                                <div class="social-follow">
                                    <a href="https://web.facebook.com/Unitedsverigese-129197640491950/" class="fb-icon" target="new"><i class="fab fa-facebook"></i></a>
                                    <a href="#" class="tw-icon" target="new"><i class="fab fa-twitter"></i></a>
                                    <a href="#" class="in-icon" target="new"><i class="fab fa-instagram"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    while (have_posts()) :
                        the_post();

                        get_template_part('template-parts/content', get_post_type());

                        //detech post view
                        wpb_set_post_views_last_7days(get_the_ID());
                        //the_post_navigation();
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>
                    <?php wp_reset_query(); ?>
                    <?php wp_reset_postdata(); ?>

                    <!--Flyer Nyther(More News) section on home page-->
                    <?php get_template_part('tmp-more-news-single'); ?>
                </div><!-- #primary -->
                <!-- sidebar start here-->
                <?php get_template_part('tmp-sidebar-single'); ?>
            </div>
        </div>
        <div class="far-right-container">
            <div class="far-right-ads-box">
                <div id='div-gpt-ad-widescreen-atf'></div>
            </div>
        </div>
    </div>
</div>