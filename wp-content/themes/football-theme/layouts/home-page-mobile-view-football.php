<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<div class="container-mobile">
    <div class="main-area">


        <div class="generic-ads-box">
<!--            <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
        </div>
        <!-- advertisement -->

        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'posts_per_page' => 5,
            'offset' => 0
        ));
        ?>

        <div class="row">

            <?php generateLargeArticle($top_posts->posts[0]); ?>

        </div> <!-- row -->

        <div class="generic-ads-box">
<!--            <img class="" src="<?php echo get_template_directory_uri() ?>/assets/images/vi-ad.png" />-->
            <?php getVIAdvertisement(); ?>
        </div><!-- advertisement -->

        <div class="row mb-1">

            <?php generateMediumArticleMobile($top_posts->posts[1]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[2]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[3]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[4]); ?>

        </div> <!-- row -->

        <div class="row">
            <div class="col-12">
                <p class="flyer-nyther-text">
                    <a href="<?php echo site_url() ?>/chelsea-news">
                        Fler nyheter från Chelsea-Nytt.se

                        <i class="fas fa-caret-right flyer-nyther-caret ml-3"></i>
                    </a>
                </p>
            </div>

            <div class="generic-ads-box">
<!--                <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
            </div><!-- advertisement -->

        </div>

        <div class="row fetched-articles-intro-section">
            <div class="col-7">
                <h4>Superbevakning</h4>
                <p>Här är allt om Chelsea ifrån alla relevanta källor på internet - hämtat i realtid. </p>
            </div>
            <div class="col-5">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/newspaper.png" />
            </div>
        </div>

        <?php
        $wpdb;

        $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 12");
        if ($skysports_articles):
            ?>


            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[0]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[1]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[2]); ?>
            </div> <!-- row -->

            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[3]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[4]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[5]); ?>

                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->
            </div> <!-- row -->

            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[6]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[7]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[8]); ?>
            </div> <!-- row -->

            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[9]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[10]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[11]); ?>
            </div> <!-- row -->

            <div class="row load-more-fetched-article-section"></div>

            <div class="row">

                <div class="col-12 load-more-posts mt-3 mb-3 load-more-fetched-article-btn" data-offset="12" data-limit="12">
                    Ladda fler artiklar
                </div><!-- load more posts-->

                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->
            </div>


        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->