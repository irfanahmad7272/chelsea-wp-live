<?php ?>

<div class="container">
    <div class="main-area grey_bg">

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images//advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->


        <div class="row">
            <?php
            while (have_posts()): the_post();
                ?>
                <div class="col-8 mb-3 single-page-main-content">
                    <div class="post feature-post">
                        <div class="post-thumbnail">
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="post image">
                        </div>
                        <div class="bg-white p-4 single-article-content">
                            <div class="social-btns d-flex">
                                <a class="btn-fb"
                                   href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                    <span class="fab fa-facebook-f"></span> Dela
                                </a>
                                <a class="btn-twitter"
                                   href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                                    <span class="fab fa-twitter"></span> Tweet
                                </a>
                            </div>
                            <div class="mb-4">
                                <span class="single-page-meta">Fobollschefen:
                                    <?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', get_the_ID())), get_the_date('Y-m-d', get_the_ID()), get_the_ID()); ?>
                                </span>
                            </div>
                            <div class="mb-4">
                                <h1 class="font-weight-bold main-title"><?php echo get_the_title() ?></h1>
                            </div>
                            <div class="single-article-excerpt">
                                <p><strong><?php echo get_the_excerpt(); ?></strong></p>
                            </div>
                            <div class="mb-4">
<!--                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement2.jpg"
                                     class="img-fluid" alt="">-->
                                <?php getVIAdvertisement(); ?>
                            </div>
                            <div class="single-article-excerpt-2">

                                <?php echo get_post_meta(get_the_ID(), 'second_excerpt', true) ?? ""; ?>

                            </div>
                            <div class="mb-4">
<!--                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/Skärmavbild_2019.png"
                                     class="img-fluid" alt="">-->
                            </div>

                            <?php echo do_shortcode(wpautop(get_the_content(), true)); ?>

                            <div class="mt-5 mb-4 article-bottom-section">
                                <p>
                                    <span>Missa inget - följ ChelseaNytt på FB och få nyheter i ditt flöde</span>
                                    <a class="fb-article-icon" href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                       onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                        <span class="fab fa-facebook-f"></span>
                                    </a>
                                </p>
                            </div>
                            <div class="followus-section d-none">
                                <div class="followus-innner d-flex">
                                    <div class="logo">
                                        <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" alt="">
                                    </div>
                                    <div class="social-section ml-auto">
                                        <div class="text mb-2">MISSA INGET. FÖLJ OSS</div>
                                        <ul class="social-bar list-inline">
                                            <li>
                                                <a href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                                    <span class="fab fa-facebook-f"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a target="new"
                                                   href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                                                    <span class="fab fa-twitter"></span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="fab fa-instagram"></span>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- col -->
                <?php
            endwhile;
            ?>
            <div class="col-md-4 single-page-sidebar">
                <div class="mb-3">
<!--                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/add.png" class="img-fluid"
                         alt="">-->
                </div>
                <?php
                $sidebar_posts = new WP_Query(array(
                    'post_type' => 'post',
                    'posts_per_page' => 3,
                    'offset' => 0,
                    'post__not_in' => array(get_the_ID())
                ));

                while ($sidebar_posts->have_posts()): $sidebar_posts->the_post();
                    ?>
                    <div class="default-post-wrap" style="min-height: auto">
                        <div class="category-name">Chelsea</div>
                        <div class="post-thumbnail">
                            <img src="<?php echo getPostImage(get_the_ID(), 'full'); ?>" alt="">
                        </div>
                        <div class="post-info default">
                            <h4 class="post-title">
                                <a href="<?php echo get_the_permalink(get_the_ID()) ?>">
                                    <?php echo get_the_title(get_the_ID()) ?>
                                </a>
                            </h4>
                        </div>
                    </div>

                <?php endwhile; ?>


            </div> <!-- col -->
        </div> <!-- row -->

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images//advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->

        <?php
        $wpdb;

        $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 12");
        if ($skysports_articles):
            ?>

            <div class="row">
                <?php generate_custom_medium_article($skysports_articles[0]); ?>
                <?php generate_custom_medium_article($skysports_articles[1]); ?>
                <?php generate_custom_medium_article($skysports_articles[2]); ?>
                <?php generate_custom_medium_article($skysports_articles[3]); ?>
                <?php generate_custom_medium_article($skysports_articles[4]); ?>
                <?php generate_custom_medium_article($skysports_articles[5]); ?>
            </div> <!-- row -->

            <div class="mb-3">
<!--                <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                     alt="">-->
            </div><!-- advertisement -->

            <div class="row">
                <?php generate_custom_medium_article($skysports_articles[6]); ?>
                <?php generate_custom_medium_article($skysports_articles[7]); ?>
                <?php generate_custom_medium_article($skysports_articles[8]); ?>
                <?php generate_custom_medium_article($skysports_articles[9]); ?>
                <?php generate_custom_medium_article($skysports_articles[10]); ?>
                <?php generate_custom_medium_article($skysports_articles[11]); ?>
            </div> <!-- row -->

            <div class="row load-more-fetched-article-section"></div>

            <div class="load-more-posts mb-3 load-more-fetched-article-btn" data-offset="12" data-limit="12">
                Ladda fler artiklar
            </div><!-- load more posts-->

        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->