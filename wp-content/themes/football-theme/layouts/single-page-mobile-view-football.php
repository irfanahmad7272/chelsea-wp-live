<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$current_post_id = get_the_ID();
?>

<div class="container-mobile container-mobile-single">
    <div class="main-area">


        <div class="generic-ads-box">
<!--            <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
        </div><!-- advertisement -->



        <div class="row">
            <?php
            while (have_posts()): the_post();
                ?>
                <div class="col-12 mb-3 single-page-main-content">
                    <div class="post feature-post">
                        <div class="post-thumbnail">
                            <img src="<?php echo get_the_post_thumbnail_url(get_the_ID(), 'full'); ?>" alt="post image">
                        </div>
                        <div class="bg-white p-4 single-article-content">
                            <div class="social-btns d-flex">
                                <a class="btn-fb"
                                   href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                    <span class="fab fa-facebook-f"></span> Dela
                                </a>
                                <a class="btn-twitter"
                                   href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                                    <span class="fab fa-twitter"></span> Tweet
                                </a>
                            </div>
                            <div class="mb-4">
                                <h1 class="font-weight-bold main-title"><?php echo get_the_title() ?></h1>
                            </div>
                            <div class="mb-4">
                                <span class="single-page-meta">Skrivet av Admin - 
                                    <?php get_post_formatted_date(strtotime(get_the_date('Y-m-d H.i', get_the_ID())), get_the_date('Y-m-d', get_the_ID()), get_the_ID()); ?>
                                </span>
                            </div>
                            <div class="single-article-excerpt">
                                <p><strong><?php echo get_the_excerpt(); ?></strong></p>
                            </div>
                            <div class="mb-4">
<!--                                    <img class="w-100" src="<?php echo get_template_directory_uri()  ?>/assets/images/vi-ad.png" />-->
                                <?php getVIAdvertisement(); ?>
                                <!-- advertisement -->
                            </div>
                            <div class="single-article-excerpt-2">

                                <?php echo get_post_meta(get_the_ID(), 'second_excerpt', true) ?? ""; ?>

                            </div>

                            <?php echo do_shortcode(wpautop(get_the_content(), true)); ?>


                        </div>
                        <div class="mt-5 mb-4 article-bottom-section">
                            <p>
                                <span>Missa inget - följ ChelseaNytt på FB</span>
                                <a class="fb-article-icon" href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                   onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                    <span class="fab fa-facebook-f"></span>
                                </a>
                            </p>
                        </div>
                    </div>
                </div> <!-- col -->
                <?php
            endwhile;
            ?>
        </div> <!-- row -->


        <div class="row mb-4">
<!--            <div class="generic-ads-box">
                <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />
            </div> advertisement -->
        </div>




        <?php
        $top_posts = new WP_Query(array(
            'post_type' => 'post',
            'post_not_in' => array($current_post_id),
            'posts_per_page' => 4,
            'offset' => 0
        ));
        ?>
        <div class="row mb-1">
            <?php generateMediumArticleMobile($top_posts->posts[0]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[1]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[2]); ?>
            <?php generateMediumArticleMobile($top_posts->posts[3]); ?>


        </div> <!-- row -->

        <div class="row">
            <div class="col-12">
                <p class="flyer-nyther-text">
                    <a href="<?php echo site_url() ?>/chelsea-news">
                        Fler nyheter från Chelsea-Nytt.se

                        <i class="fas fa-caret-right flyer-nyther-caret ml-3"></i>
                    </a>
                </p>
            </div>

            <div class="generic-ads-box">
<!--                <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
            </div><!-- advertisement -->

        </div>

        <div class="row fetched-articles-intro-section">
            <div class="col-7">
                <h4>Superbevakning</h4>
                <p>Här är allt om Chelsea ifrån alla relevanta källor på internet - hämtat i realtid. </p>
            </div>
            <div class="col-5">
                <img src="<?php echo get_template_directory_uri() ?>/assets/images/newspaper.png" />
            </div>
        </div>

        <?php
        $wpdb;

        $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 3");
        if ($skysports_articles):
            ?>


            <div class="row">
                <?php generate_custom_medium_article_mobile($skysports_articles[0]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[1]); ?>
                <?php generate_custom_small_article_mobile($skysports_articles[2]); ?>
            </div> <!-- row -->

            <div class="row load-more-fetched-article-section"></div>

            <div class="row">
                <div class="col-12 load-more-posts mt-3 mb-3 load-more-fetched-article-btn" data-offset="3" data-limit="12">
                    Ladda fler artiklar
                </div><!-- load more posts-->

                <div class="generic-ads-box">
<!--                    <img class="img-320" src="<?php echo get_template_directory_uri() ?>/assets/images/mobile-ad.jpg" />-->
                </div><!-- advertisement -->
            </div>


        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->