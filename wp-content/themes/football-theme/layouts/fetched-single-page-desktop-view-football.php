<?php

 global $wpdb;
 $article_id = isset($_GET['id']) ? $_GET['id'] : "";
 $article = $wpdb->get_row("select * from articles where id = {$article_id}");

?>
<div class="container">
    <div class="main-area grey_bg">

        <div class="mb-3">
<!--            <img src="<?php echo site_url() ?>/wp-content/uploads/2020/01/advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->

        <div class="row">
            <?php
                while(have_posts()): the_post();
            ?>
            <div class="col-8 mb-3 single-page-main-content">
                <div class="post feature-post">
                    <div class="p-4 bg-white row" style="margin: 0 !important;">
                        <div class="col-md-6 col-xs-12">
                            <div class="post-thumbnail fetched-article-image single-site-logo">
                                <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo-blue.png" />
                            </div>
                        </div>
                        <div class="col-md-6 col-xs-12">
                            <div class="post-thumbnail fetched-article-image">
                                <img src="<?php echo $article->img_url ?>" alt="post image">
                            </div>
                        </div>
                    </div>

                    <div class="bg-white p-4 single-article-content">
                        <div class="social-btns d-flex">
                            <a class="btn-fb"
                                href="https://www.facebook.com/sharer/sharer.php?sdk=joey&u=<?php echo get_the_permalink() ?>&display=popup&ref=plugin&src=share_button"
                                onclick="return !window.open(this.href, 'Facebook', 'width=640,height=580')">
                                <span class="fab fa-facebook-f"></span> Dela
                            </a>
                            <a class="btn-twitter"
                                href="https://twitter.com/intent/tweet?url=<?= urlencode(get_the_permalink()) ?>">
                                <span class="fab fa-twitter"></span> Tweet
                            </a>
                        </div>
                        <div class="mb-4">
                            <span class="single-page-meta">Publicerad: <?php echo $article->source_name ?> - 
                                <?php get_article_formatted_date(strtotime($article->posted_date), $article->posted_date); ?>
                            </span>
                        </div>
                        <div class="mb-4">
                            <h1 class="font-weight-bold main-title"><?php echo $article->title ?></h1>
                        </div>
                        <div class="mb-4">
<!--                            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement2.jpg"
                                class="img-fluid" alt="">-->
                            <?php getVIAdvertisement(); ?>
                        </div>
                        <div class="mt-5 mb-5">
                            <p><?php echo $article->description ?></p>
                        </div>
                        
                        <div class="col-12 followus-section-fetched followus-feteched-desktop">
                            <div class="row">
                                <div class="social-section col-7">
                                    <div class="text">
                                        <a class="source-link" target="_blank" href="<?php echo $article->url ?>">Läs hela nyheten  </a>
                                        <i class=" text-danger fas fa-caret-right"></i>
                                    </div>

                                </div>
                                <div class="logo-source col-5 pt-1 text-right">
                                    <a target="_blank" href="<?php echo $article->url ?>">
                                        <img src="<?php echo get_article_source_img_url($article->source_name) ?>"
                                            alt="">
                                    </a>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div> <!-- col -->
            <?php
                endwhile;
            ?>
            <div class="col-md-4 single-page-sidebar">
                <div class="mb-3">
<!--                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/add.png" class="img-fluid"
                        alt="">-->
                </div>
                <?php
                    $sidebar_posts = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 2,
                        'offset' => 0,
                        'post__not_in' => array(get_the_ID())
                    ));

                    while($sidebar_posts->have_posts()): $sidebar_posts->the_post();

                ?>
                <div class="default-post-wrap" style="min-height: auto">
                    <div class="category-name">Chelsea</div>
                    <div class="post-thumbnail">
                        <img src="<?php echo getPostImage(get_the_ID(), 'full'); ?>" alt="">
                    </div>
                    <div class="post-info default">
                        <h4 class="post-title">
                            <a href="<?php echo get_the_permalink(get_the_ID()) ?>">
                                <?php echo get_the_title(get_the_ID()) ?>
                            </a>
                        </h4>
                    </div>
                </div>

                <?php endwhile; ?>


            </div> <!-- col -->
        </div> <!-- row -->

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid" alt="">-->
        </div><!-- advertisement -->

        <?php
        
        $wpdb;
        
         $skysports_articles = $wpdb->get_results("select * from articles where team_name = 'Chelsea' order by posted_date DESC limit 12");
         if($skysports_articles):
        
        ?>

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[0]); ?>
            <?php generate_custom_medium_article($skysports_articles[1]); ?>
            <?php generate_custom_medium_article($skysports_articles[2]); ?>
            <?php generate_custom_medium_article($skysports_articles[3]); ?>
            <?php generate_custom_medium_article($skysports_articles[4]); ?>
            <?php generate_custom_medium_article($skysports_articles[5]); ?>
        </div> <!-- row -->

        <div class="mb-3">
<!--            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">-->
        </div><!-- advertisement -->

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[6]); ?>
            <?php generate_custom_medium_article($skysports_articles[7]); ?>
            <?php generate_custom_medium_article($skysports_articles[8]); ?>
            <?php generate_custom_medium_article($skysports_articles[9]); ?>
            <?php generate_custom_medium_article($skysports_articles[10]); ?>
            <?php generate_custom_medium_article($skysports_articles[11]); ?>
        </div> <!-- row -->
        
        <div class="row load-more-fetched-article-section"></div>
        
        <div class="load-more-posts mb-3 load-more-fetched-article-btn" data-offset="12" data-limit="12">
            Ladda fler artiklar
        </div><!-- load more posts-->

        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->