<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$posts_offset = 35;
$increment = 4;
$more_posts = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => 4,
    'offset' => $posts_offset
        ));
?>
<div class="more-news-section" data-offset="<?php echo $posts_offset ?>" data-increment="<?php echo $increment ?>">
    <h3 class="more-news-title d-none d-sm-block">Fler Nyheter</h3>
    <?php if ($more_posts->have_posts()): ?>
        <?php foreach ($more_posts->posts as $post): ?>
            <?php generateMoreArticle($post); ?>
        <?php endforeach; ?>
    <?php endif; ?>
</div>