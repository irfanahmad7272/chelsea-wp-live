<?php 
$current_post_ID = get_the_ID();
?>
<div class="container single-post-page">
    <div class="main-site-container">
        <div class="centeral-container">
            <div class="row site-top-row site-top-row-sticky">
                <div class="generic-ads-box d-block d-sm-none single-page-top-sticky-ad-mobile sticky-top-ad">
                    <div id="single-page-top-sticky-ad-wrapper">
                        <div id='div-gpt-ad-panorama-0'>
                            <script data-adfscript="adx.adform.net/adx/?mid=555304"></script>
                            <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                        </div>
                    </div>
                </div>
            </div> <!-- row -->
            <div class="row">
                <div class="col-lg-8 col-12 main-side">
                    <?php
                    while (have_posts()) :
                        the_post();

                        get_template_part('template-parts/content-mobile', get_post_type());

                        //detech post view
                        wpb_set_post_views_last_7days(get_the_ID());
                        //the_post_navigation();
                        // If comments are open or we have at least one comment, load up the comment template.
                        if (comments_open() || get_comments_number()) :
                            comments_template();
                        endif;

                    endwhile; // End of the loop.
                    ?>
                    <?php wp_reset_query(); ?>
                    <?php wp_reset_postdata(); ?>

                    <!--Flyer Nyther(More News) section on home page-->
                    <div class="generic-ads-box d-block d-sm-none">
                        <div id='div-gpt-ad-panorama-2'>
                            <script data-adfscript="adx.adform.net/adx/?mid=555306"></script>
                            <script src="//s1.adform.net/banners/scripts/adx.js" async defer></script>
                        </div>
                    </div>
                    <div class="related_articles_section">
                        <?php if (get_post_meta(get_the_ID(), 'external_link_1_title', true)): ?>
                            <h6>Ämnet i andra tidningar</h6>
                        <?php endif; ?>
                        <ul class="related_articles_list list-inline">
                            <?php if (get_post_meta(get_the_ID(), 'external_link_1_title', true)): ?>
                                <li>
                                    <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_1', true) ?? ""; ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'external_link_1_title', true) ?? ""; ?>
                                        <span><?php echo get_post_meta(get_the_ID(), 'external_link_1_source', true) ?? ""; ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_post_meta(get_the_ID(), 'external_link_2_title', true)): ?>
                                <li>
                                    <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_2', true) ?? ""; ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'external_link_2_title', true) ?? ""; ?>
                                        <span><?php echo get_post_meta(get_the_ID(), 'external_link_2_source', true) ?? ""; ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_post_meta(get_the_ID(), 'external_link_3_title', true)): ?>
                                <li>
                                    <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_3', true) ?? ""; ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'external_link_3_title', true) ?? ""; ?>
                                        <span><?php echo get_post_meta(get_the_ID(), 'external_link_3_source', true) ?? ""; ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_post_meta(get_the_ID(), 'external_link_4_title', true)): ?>
                                <li>
                                    <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_4', true) ?? ""; ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'external_link_4_title', true) ?? ""; ?>
                                        <span><?php echo get_post_meta(get_the_ID(), 'external_link_4_source', true) ?? ""; ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if (get_post_meta(get_the_ID(), 'external_link_5_title', true)): ?>
                                <li>
                                    <a target="new" href="<?php echo get_post_meta(get_the_ID(), 'external_link_5', true) ?? ""; ?>">
                                        <?php echo get_post_meta(get_the_ID(), 'external_link_5_title', true) ?? ""; ?>
                                        <span><?php echo get_post_meta(get_the_ID(), 'external_link_5_source', true) ?? ""; ?></span>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="d-block d-sm-none">
                        <?php
                        //home page view for single article page.
                        $top_posts = new WP_Query(array(
                            'post_type' => 'post',
                            'posts_per_page' => 3,
                            'offset' => 0,
                            'post__not_in' => array($current_post_ID)
                        ));

                        if ($top_posts->have_posts()):
                            ?>
                            <?php generateLargeArticle($top_posts->posts[0]); ?>
                            <?php generateLargeArticle($top_posts->posts[1]); ?>
                            <?php generateLargeArticle($top_posts->posts[2]); ?>
                            <div class="generic-ads-box">

                            </div>

                            <?php
                        endif;
                        ?>
                    </div>
                    <!--  Flyer nyther section -->
                    <?php
                    //flyer nyther section for single article page.
                    //$current_post_ID = get_the_ID();
                    $posts_offset = 3;
                    $increment = 4;
                    $more_posts = new WP_Query(array(
                        'post_type' => 'post',
                        'posts_per_page' => 4,
                        'offset' => $posts_offset,
                        'post_status' => 'publish',
                        'post__not_in' => array($current_post_ID)
                    ));
                    ?>
                    <div class="more-news-section" data-offset="<?php echo $posts_offset + 1 ?>" data-increment="<?php echo $increment ?>" data-current="<?php echo $current_post_ID ?>">
                        <?php if ($more_posts->have_posts()): ?>
                            <?php foreach ($more_posts->posts as $post): ?>
                                <?php generateMoreArticle($post); ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </div>
                </div><!-- #primary -->
            </div>
        </div>
    </div>
</div>