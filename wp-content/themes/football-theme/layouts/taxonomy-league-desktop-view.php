<?php

$league_term = get_queried_object();
$league_image = get_term_meta($league_term->term_id, 'league_header_image', true);
$article_db_query = "";
if($league_term->parent !== 0 ){

    $team = get_term_by('id', $league_term->parent, 'league');
    $article_db_query = "select * from articles where team_name = '{$league_term->name}' and league_name = '{$team->name}' order by posted_date DESC limit 32";

}else{

    $article_db_query = "select * from articles where league_name = '{$league_term->name}' order by posted_date DESC limit 32";
}
?>

<div class="container">
    <div class="main-area grey_bg">

        <div class="mb-3">
            <img src="<?php echo site_url() ?>/wp-content/uploads/2020/01/advertisement.jpg" class="img-fluid" alt="">
        </div><!-- advertisement -->

        <?php if($league_image): ?>
        <div class="mb-3">
            <img src="<?php echo $league_image['guid'] ?>" class="img-fluid league-header-img" alt="">
        </div>
        <?php endif;?>

        <?php
        
        $wpdb;
        
         $skysports_articles = $wpdb->get_results($article_db_query);
         if($skysports_articles):
        
        ?>

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[0]); ?>
            <?php generate_custom_medium_article($skysports_articles[1]); ?>
            <?php generate_custom_medium_article($skysports_articles[2]); ?>
            <?php generate_custom_medium_article($skysports_articles[3]); ?>
            <?php generate_custom_medium_article($skysports_articles[4]); ?>
            <?php generate_custom_medium_article($skysports_articles[5]); ?>
        </div> <!-- row -->

        <div class="mb-3">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">
        </div><!-- advertisement -->


        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[6]); ?>
            <?php generate_custom_medium_article($skysports_articles[7]); ?>
            <?php generate_custom_medium_article($skysports_articles[8]); ?>
            <?php generate_custom_medium_article($skysports_articles[9]); ?>
            <?php generate_custom_medium_article($skysports_articles[10]); ?>
            <?php generate_custom_medium_article($skysports_articles[11]); ?>
        </div> <!-- row -->

        <div class="mb-3">
            <img src="<?php echo get_template_directory_uri() ?>/assets/images/advertisement.jpg" class="img-fluid"
                alt="">
        </div><!-- advertisement -->

        <div class="row">
            <?php generate_custom_medium_article($skysports_articles[12]); ?>
            <?php generate_custom_medium_article($skysports_articles[13]); ?>
            <?php generate_custom_medium_article($skysports_articles[14]); ?>
            <?php generate_custom_medium_article($skysports_articles[15]); ?>
            <?php generate_custom_medium_article($skysports_articles[16]); ?>
            <?php generate_custom_medium_article($skysports_articles[17]); ?>
        </div> <!-- row -->


        <div class="load-more-posts">
            Ladda fler artiklar (Load more articles)
        </div><!-- load more posts-->

        <?php endif; ?>

    </div> <!-- main-area -->
</div> <!-- container -->