<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package United_Theme
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width,height=device-height, initial-scale=1">
        <link rel="canonical" href="/">
        <meta property="og:url"           content="<?php echo get_the_permalink() ?>" />
        <meta property="og:type"          content="website" />
        <meta property="og:title"         content="<?php echo get_the_title() ?>" />
        <meta property="og:description"   content="<?php echo wp_trim_words($post->post_content, 20) ?>" />
        <meta property="og:image"         content="<?php echo getPostImage($post->ID, 'medium'); ?>" />
        <link rel="profile" href="https://gmpg.org/xfn/11">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Titillium+Web:300,400,600,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Comfortaa:500" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/bootstrap.min.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/style.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/owl/owl.carousel.min.css" />
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets-old/css/theme-style.css" />
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-20879025-1');
        </script>
        <?php wp_head(); ?>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-20879025-1"></script>
        

        <script type="text/javascript" async=true>
            var elem = document.createElement('script');
            elem.src = 'https://quantcast.mgr.consensu.org/cmp.js';
            elem.async = true;
            elem.type = "text/javascript";
            var scpt = document.getElementsByTagName('script')[0];
            scpt.parentNode.insertBefore(elem, scpt);
            (function () {
                var gdprAppliesGlobally = true;
                function addFrame() {
                    if (!window.frames['__cmpLocator']) {
                        if (document.body) {
                            var body = document.body,
                                    iframe = document.createElement('iframe');
                            iframe.style = 'display:none';
                            iframe.name = '__cmpLocator';
                            body.appendChild(iframe);
                        } else {
                            // In the case where this stub is located in the head,
                            // this allows us to inject the iframe more quickly than
                            // relying on DOMContentLoaded or other events.
                            setTimeout(addFrame, 5);
                        }
                    }
                }
                addFrame();
                function cmpMsgHandler(event) {
                    var msgIsString = typeof event.data === "string";
                    var json;
                    if (msgIsString) {
                        json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};
                    } else {
                        json = event.data;
                    }
                    if (json.__cmpCall) {
                        var i = json.__cmpCall;
                        window.__cmp(i.command, i.parameter, function (retValue, success) {
                            var returnMsg = {"__cmpReturn": {
                                    "returnValue": retValue,
                                    "success": success,
                                    "callId": i.callId
                                }};
                            event.source.postMessage(msgIsString ?
                                    JSON.stringify(returnMsg) : returnMsg, '*');
                        });
                    }
                }
                window.__cmp = function (c) {
                    var b = arguments;
                    if (!b.length) {
                        return __cmp.a;
                    } else if (b[0] === 'ping') {
                        b[2]({"gdprAppliesGlobally": gdprAppliesGlobally,
                            "cmpLoaded": false}, true);
                    } else if (c == '__cmp')
                        return false;
                    else {
                        if (typeof __cmp.a === 'undefined') {
                            __cmp.a = [];
                        }
                        __cmp.a.push([].slice.apply(b));
                    }
                }
                window.__cmp.gdprAppliesGlobally = gdprAppliesGlobally;
                window.__cmp.msgHandler = cmpMsgHandler;
                if (window.addEventListener) {
                    window.addEventListener('message', cmpMsgHandler, false);
                } else {
                    window.attachEvent('onmessage', cmpMsgHandler);
                }
            })();
            window.__cmp('init', {
                'Language': 'sv',
                'Initial Screen Title Text': 'Vi värdesätter din integritet',
                'Initial Screen Reject Button Text': 'Jag godkänner inte',
                'Initial Screen Accept Button Text': 'Jag godkänner',
                'Initial Screen Purpose Link Text': 'Visa ändamål',
                'Purpose Screen Title Text': 'Vi värdesätter din integritet',
                'Purpose Screen Body Text': 'Du kan ange dina samtyckespreferenser och avgöra hur du vill att dina data ska användas utifrån ändamålen nedan. Du kan ange separata preferenser för oss och tredje parter. Varje ändamål har en beskrivning så att du kan se hur vi och våra samarbetspartner använder dina uppgifter.',
                'Purpose Screen Vendor Link Text': 'Visa säljare',
                'Purpose Screen Cancel Button Text': 'Avbryt',
                'Purpose Screen Save and Exit Button Text': 'Spara och Avsluta',
                'Vendor Screen Title Text': 'Vi värdesätter din integritet',
                'Vendor Screen Body Text': 'Du kan ange samtyckespreferenser för de enskilda tredje parter vi samarbetar med nedan. Du kan maximera informationen om företagen på listan för att se för vilka ändamål de använder uppgifter och på så sätt få hjälp att göra dina val. I vissa fall kan företag använda dina data utan att be om ditt samtycke, med stöd av sina legitima intressen. Du kan klicka på länkarna till deras integritetspolicyer för att få mer information och för att neka till sådan användning.',
                'Vendor Screen Accept All Button Text': 'GODKÄNN ALLA',
                'Vendor Screen Reject All Button Text': 'AVVISA ALLA',
                'Vendor Screen Purposes Link Text': 'Tillbaka till ändamål',
                'Vendor Screen Cancel Button Text': 'Avbryt',
                'Vendor Screen Save and Exit Button Text': 'Spara och Avsluta',
                'Initial Screen Body Text': 'Vi och våra samarbetspartner använder teknologi såsom cookies för att behandla personuppgifter, exempelvis IP-adresser och cookie-identifierare, för att anpassa annonser och innehåll enligt dina intressen, mäta effektiviteten hos annonser och innehåll samt härleda information om de personer som sett annonserna och innehållet. Klicka nedan för att godkänna användningen av denna teknologi och behandling av dina personuppgifter för dessa ändamål. Du kan när som helst ändra dina val gällande samtycke genom att återkomma till denna sida.',
                'Initial Screen Body Text Option': 1,
                'Min Days Between UI Displays': 365,
                'Publisher Name': 'unitedsverige',
                'Publisher Logo': 'http://idoweb.se/united-beta/wp-content/uploads/2017/03/manchesterUnitedLogo.png',
                'Post Consent Page': 'http://unitedsverige.se/',
                'Consent Scope': 'service',
                'UI Layout': 'banner',
                'Non-Consent Display Frequency': 365,
                'No Option': false,
            });
        </script>
        <script async type='text/javascript' src='//hbid.ams3.cdn.digitaloceanspaces.com/user_js/108_1038.js'></script>
        <script type="text/javascript" async="true">
            window._taboola = window._taboola || [];
            _taboola.push({article: 'auto'});
            !function (e, f, u, i) {
                if (!document.getElementById(i)) {
                    e.async = 1;
                    e.src = u;
                    e.id = i;
                    f.parentNode.insertBefore(e, f);
                }
            }(document.createElement('script'),
                    document.getElementsByTagName('script')[0],
                    '//cdn.taboola.com/libtrc/unitedsverige/loader.js',
                    'tb_loader_script');
            if (window.performance && typeof window.performance.mark == 'function')
            {
                window.performance.mark('tbl_ic');
            }
        </script>
    </head>

    <body <?php body_class(); ?>>
        <div id="fb-root"></div>
        <script async defer crossorigin="anonymous" src="https://connect.facebook.net/sv_SE/sdk.js#xfbml=1&version=v3.2"></script>
        <header class="header">
            <div class="container">
                <div class="centeral-container">
                    <div class="row align-items-center site-header-row">
                        <div class="col-md-3 col-12">
                            <div class="logo">
                                <a href="<?php echo site_url() ?>">
                                    <img src="<?php echo get_template_directory_uri() ?>/assets/images/logo.png" alt="">
                                </a>
                            </div>
                        </div><!-- col -->
                        <div class="col-md-9 col-12 d-none d-sm-block">
                            <div class="d-flex align-items-center">
                                <ul class="header-links d-none list-inline">
                                    <li><a href="#">United pa TV</a></li>
                                    <li><a href="#">United pa Internet</a></li>
                                </ul>
                                <div class="search">
                                    <form action="<?php echo site_url() ?>/search" method="get">
                                        <i class="fa fa-search"></i>
                                        <input name="q" type="text" class="form-control" placeholder="Search...">
                                    </form>
                                </div>
                                <div class="loginbtn ml-auto d-none">
                                    <a href="#">Login <span><i class="fa fa-user"></i></span></a>
                                </div>
                            </div>
                        </div> <!-- col -->
                    </div><!-- row -->
                </div>
                <div class="far-right-container">

                </div>
            </div><!-- container -->
        </header> 
        <div class="main">
