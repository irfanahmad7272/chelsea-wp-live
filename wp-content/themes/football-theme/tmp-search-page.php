<?php
/*
 * Template Name: Search Page
 */

get_header();

$search_query = $_GET['q'];
$posts_offset = 0;
$increment = 10;
$search_posts = new WP_Query(array(
    'post_type' => 'post',
    'posts_per_page' => -1,
    's' => $search_query
        ));
?>


<div class="container search-page-template">
    <div class="row">
        <div class="col-lg-8 col-12 main-side">
            <div class="more-search-news-section" data-offset="<?php echo $posts_offset ?>" data-increment="<?php echo $increment ?>" data-query="<?php echo $search_query ?>">
                <h3 class="more-news-title">SÖKRESULTAT FÖR: <?php echo $search_query ?></h3>
                <?php if ($search_posts->have_posts()): ?>
                    <?php foreach ($search_posts->posts as $post): ?>
                        <?php generateMoreArticle($post); ?>
                    <?php endforeach; ?>
                <?php endif; ?>
            </div>

        </div><!-- #primary -->
        <!-- sidebar start here-->
        <?php include_once 'tmp-sidebar.php'; ?>
    </div>
</div>








<?php
get_footer();
