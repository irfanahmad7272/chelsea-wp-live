<div class="col-lg-4 d-none d-lg-block sidebar-section">
    <div class="side-section">
        <div class="followUs">
            <div class="d-flex align-items-center">
                <h6 class="d-none d-sm-block">Missa inget. Följ Unitedsverige</h6>
                <div class="ml-auto social-follow-container">
                    <div class="social-follow">
                        <a href="https://web.facebook.com/Unitedsverigese-129197640491950/" class="fb-icon" target="new"><i class="fab fa-facebook"></i></a>
                        <a href="#" class="tw-icon" target="new"><i class="fab fa-twitter"></i></a>
                        <a href="#" class="in-icon" target="new"><i class="fab fa-instagram"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="side-section">
        <div class="generic-ads-box">
            <div id='div-gpt-ad-insider-atf'></div>
        </div>
    </div>
    <div class="side-section">
        <div class="section-title">
            <h6>Mest lästa senaste veckan</h6>
        </div>
        <?php
        $most_read_posts = new WP_Query(array(
            'post_type' => 'post',
            'meta_key' => 'view_recent_count',
            'orderby' => 'meta_value_num',
            'order' => 'DESC',
            'posts_per_page' => 3
        ));
        ?>
        <ul class="favourte_post_list">
            <?php foreach ($most_read_posts->posts as $post): ?>
                <li><?php //echo get_post_meta($post->ID, 'view_recent_count', true) ?>
                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                        <span class="thumbnail">
                            <img src="<?php echo getPostImage($post->ID, 'thumbnail'); ?>" alt="<?php echo get_the_title($post->ID) ?>" />
                        </span>
                        <span class="s-title"><?php echo get_the_title($post->ID) ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div> <!-- sidebar section -->
    <div class="side-section">
        <div class="video-ad-single-article-desktop">
            
        </div>
    </div>
    <div class="side-section">
        <div class="generic-ads-box">
            <div id='div-gpt-ad-widescreen-0'></div>
        </div>
    </div>
    <div class="side-section" style="margin-top: 30px;">
       <iframe src="https://www.tvmatchen.nu/widget/5d614495c2662?heading=TV-matcher&border_color=custom&autoscroll=1&custom_colors=ffffff,ffffff,00001e" frameborder="0" style="width: 100%; height: 400px; border: none"></iframe>
       <p style="margin-top: 2px; font-size: 10px;">&copy; <a href="https://www.tvmatchen.nu?link_src=widget&ref=http://unitedsverige.se" target="_blank">TVmatchen.nu - Fotboll på TV</a></p>
    </div> 
    <div class="side-section">
        <div class="generic-ads-box">
            <div id='div-gpt-ad-widescreen-1'></div>
        </div>
    </div>
    <div class="side-section">
        <div class="section-title">
            <h6>Rekommenderat</h6>
        </div>
        <?php
        $most_recom_posts = new WP_Query(array(
            'post_type' => 'post',
            'meta_key' => '_is_ns_featured_post',
            'meta_value' => 'yes',
            'orderby' => 'date',
            'order' => 'DESC',
            'posts_per_page' => 7
        ));
        ?>
        <ul class="favourte_post_list">
            <?php foreach ($most_recom_posts->posts as $post): ?>
                <li>
                    <a href="<?php echo get_the_permalink($post->ID) ?>">
                        <span class="thumbnail">
                            <img src="<?php echo getPostImage($post->ID, 'thumbnail'); ?>" alt="<?php echo get_the_title($post->ID) ?>" />
                        </span>
                        <span class="s-title"><?php echo get_the_title($post->ID) ?></span>
                    </a>
                </li>
            <?php endforeach; ?>
        </ul>
    </div> <!-- sidebar section -->
    <div class="side-section right-side-last-ad">
        <div class="generic-ads-box">
            <div id='div-gpt-ad-widescreen-btf'></div>
        </div>
    </div>
</div>