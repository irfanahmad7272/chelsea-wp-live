<?php
/*
 * Template for custom league taxonomy terms
 */

get_header();

if (isMobileDevice()) {
    include_once 'layouts/taxonomy-league-mobile-view.php';
} else {

    include_once 'layouts/taxonomy-league-desktop-view.php';
}
?>


<?php
get_footer();