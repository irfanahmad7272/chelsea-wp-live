<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$most_read_posts_mobile = new WP_Query(array(
    'post_type' => 'post',
    'meta_key' => 'view_recent_count',
    'orderby' => 'meta_value_num',
    'order' => 'DESC',
    'posts_per_page' => 7
        ));
?>
<div class="favourte_post_list_slider">
    
    <div class="owl-carousel owl-theme">
        <?php foreach ($most_read_posts_mobile->posts as $post): ?>
            <div class="item">
                <a href="<?php echo get_the_permalink($post->ID) ?>">
                    <span class="most-read-slider-title">Mest lästa senaste veckan</span>
                    <img src="<?php echo getPostImage($post->ID, 'large'); ?>" alt="<?php echo get_the_title($post->ID) ?>" />
                    <span class="title owl-slider-title"><?php echo get_the_title($post->ID) ?></span>
                </a>
            </div>
        <?php endforeach; ?>
    </div>
</div>

